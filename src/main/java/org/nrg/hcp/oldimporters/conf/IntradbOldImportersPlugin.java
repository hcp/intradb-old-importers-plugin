package org.nrg.hcp.oldimporters.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbOldImportersPlugin",
			name = "Intradb Old Importers Plugin"
		)
@ComponentScan({ 
	"org.nrg.hcp.oldimporters.components"
	})
public class IntradbOldImportersPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbOldImportersPlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public IntradbOldImportersPlugin() {
		logger.info("Configuring IntraDB old importers plugin");
	}
	
}
