package org.nrg.hcp.importer;

//******************************************//
// NOTE:  This importer is no longer in use //
//******************************************//

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.log4j.Logger;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.dcm.DicomUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.CatCatalog;
import org.nrg.xdat.om.CatEntry;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xnat.restlet.actions.importer.ImporterHandler;
import org.nrg.xnat.restlet.actions.importer.ImporterHandlerA;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import java.util.zip.ZipOutputStream;

import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xft.utils.zip.TarUtils;
import org.nrg.xft.utils.zip.ZipI;
import org.nrg.xft.utils.zip.ZipUtils;
import org.mozilla.universalchardet.UniversalDetector;

/**
 * Matches EPRIME files to scans and uploads as a scan resource
 * @author Mike Hodge <hodgem@mir.wustl.edu>
 *
 */
@ImporterHandler(handler = "EPRIME", allowCallsWithoutFiles = true, callPartialUriWrap = false)
public class HCPEprimeImporter extends ImporterHandlerA implements Callable<List<String>> {

	static final String[] zipExtensions={".zip",".jar",".rar",".ear",".gar",".xar"};
	static final String[] boldTypes={"tfMRI"};
	static final String dcmExtension = ".dcm";
	// E-prime file recieved thus far have been 16-bit 
	static final String defaultEncoding = "UTF-16LE";
	static final String EVENT_REASON = "Upload E-prime Files";

	static Logger logger = Logger.getLogger(HCPEprimeImporter.class);

	private final FileWriterWrapperI fw;
	private final UserI user;
	final Map<String,Object> params;
   	private XnatProjectdata proj;
   	private XnatMrsessiondata exp;
	private List<String> returnList = new ArrayList<String>();;
   	
	/**
	 * 
	 * @param listenerControl
	 * @param u
	 * @param session
	 * @param overwrite:   'append' means overwrite, but preserve un-modified content (don't delete anything)
	 *                      'delete' means delete the pre-existing content.
	 * @param additionalValues: should include project (subject and experiment are expected to be found in the archive)
	 */
	public HCPEprimeImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u);
		this.user=u;
		this.fw=fw;
		this.params=params;
	}

	@Override
	public List<String> call() throws ClientException, ServerException {
		verifyAndGetExperiment();
		try {
			processUpload();
			this.completed("Success");
			return returnList;
		} catch (ClientException e) {
			logger.error("",e);
			this.failed(e.getMessage());
			throw e;
		} catch (ServerException e) {
			logger.error("",e);
			this.failed(e.getMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("",e);
			throw new ServerException(e.getMessage(),new Exception());
		}
	}

	private void verifyAndGetExperiment() throws ClientException {
		String projID=null;
		if (params.get("project")!=null) {
			projID=params.get("project").toString();
		}
		String subjLbl=null;
		if (params.get("subject")!=null) {
			subjLbl=params.get("subject").toString();
		}
		if (params.get("experiment") == null) {
			clientFailed("ERROR:  experiment parameter (containing experiment label) must be supplied for import");
		}
		String expLbl=params.get("experiment").toString();
		
        this.exp =XnatMrsessiondata.getXnatMrsessiondatasById(expLbl, user, false);
        if(exp==null){
        	ArrayList<XnatMrsessiondata> al=XnatMrsessiondata.getXnatMrsessiondatasByField("xnat:mrSessionData/label",expLbl, user, false);
        	// Using iterator here because removing within for/next can be unpredictable
        	Iterator<XnatMrsessiondata> aIter = al.iterator();
        	while (aIter.hasNext()) {
        		XnatMrsessiondata mrsess = aIter.next();
        		if (projID!=null && !mrsess.getProject().equalsIgnoreCase(projID)) {
        			aIter.remove();
        			continue;
        		}
        		if (subjLbl!=null && 
        				!(mrsess.getSubjectData().getId().equalsIgnoreCase(subjLbl) || mrsess.getSubjectData().getLabel().equalsIgnoreCase(subjLbl))) {
        			aIter.remove();
        			continue;
        		}
        		exp=mrsess;
        	}
        	if (al.size()>1) {
        		clientFailed("ERROR:  Multiple sessions match passed parameters. Consider using experiment assession number or supplying project/subject parameters ");
        	}
        }
       	if (exp==null) {
        	clientFailed("ERROR:  Could not find matching experiment for import");
        }
       	proj=exp.getProjectData();
	}
	

	private void clientFailed(String fmsg) throws ClientException {
		this.failed(fmsg);
		throw new ClientException(fmsg,new Exception());
	}

	private void processUpload() throws ClientException,ServerException {
		
		String cachePath = ArcSpecManager.GetInstance().getGlobalCachePath();
		
		boolean doProcess = false;
		// Multiple uploads are allowed to same space (processing will take place when process parameter=true).  Use specified build path when
		// one is given, otherwise create new one
		String specPath=null;
		boolean invalidSpecpath = false;
		if (params.get("buildPath")!=null) {
			// If buildpath parameter is specified and valid, use it
			specPath=params.get("buildPath").toString();
			if (specPath.indexOf(cachePath)>=0 && specPath.indexOf("user_uploads")>=0 &&
					specPath.indexOf(File.separator + user.getID() + File.separator)>=0 && new File(specPath).isDirectory()) {
				cachePath=specPath;
			} else {
				specPath=null;
				invalidSpecpath = true;
			}
		} 
		if (specPath==null) {
			Date d = Calendar.getInstance().getTime();
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ("yyyyMMdd_HHmmss");
			String uploadID = formatter.format(d);
			// Save input files to cache space
			cachePath+="user_uploads/"+user.getID() + "/" + uploadID + "/";
		}
		// If uploading zip file with no process or build directory specified, will proceed with processing regardless unless told not 
		// to via process parameter.  Otherwise will only process when told to (when done uploading).
		String processParm = null;
		if (params.get("process")!=null) {
			processParm = params.get("process").toString();
		}
		
		final File cacheLoc = new File(cachePath);
		cacheLoc.mkdirs();
		
		// If uploading a file, process it.  Otherwise just set doProcess parameter (default=true)
		if (fw!=null) {
			doProcess=processFile(cacheLoc, specPath, processParm, doProcess, invalidSpecpath);
		} else {
			if (!(processParm!=null && (processParm.equalsIgnoreCase("NO") || processParm.equalsIgnoreCase("FALSE")))) {
				doProcess=true;
			}
		}
		
		// Conditionally process cache location files, otherwise return cache location
		if (doProcess) {
			processCacheFiles(cacheLoc);
		} else {
			returnList.add(cachePath);
		}
		
	}

	private boolean processFile(File cacheLoc, String specPath, String processParm, boolean doProcess, boolean invalidSpecpath) throws ClientException {
		final String fileName = fw.getName();
		if (specPath==null && isZipFileName(fileName)) {
			if (!(processParm!=null && (processParm.equalsIgnoreCase("NO") || processParm.equalsIgnoreCase("FALSE")))) {
				doProcess=true;
			}
		// If not uploading a zip file, only process when told to (via processParm) 
		} else if (!isZipFileName(fileName)) {
			if (processParm!=null && (processParm.equalsIgnoreCase("YES") || processParm.equalsIgnoreCase("TRUE"))) {
				doProcess=true;
			}
		}
		
		if (invalidSpecpath && !doProcess) {
			throw new ClientException("ERROR:  Specified build path is invalid");
		}
        
		if (isZipFileName(fileName)) {
			ZipI zipper = getZipper(fileName);
			try {
	        	zipper.extract(fw.getInputStream(),cacheLoc.getAbsolutePath());
	        } catch (Exception e) {
	        	throw new ClientException("Archive file is corrupt or not a valid archive file type.");
	        }
		} else {
			File cacheFile = new File(cacheLoc,fileName);
			try {
				FileOutputStream fout = new FileOutputStream(cacheFile);
				OutputStreamWriter writer; 
				if (fileName.toLowerCase().endsWith(".edat2")) {
					// Binary Copy
					InputStream fin = fw.getInputStream();
					int noOfBytes = 0;
                    byte[] b = new byte[1024];
					while ((noOfBytes = fin.read(b))!=-1) {
						fout.write(b,0,noOfBytes);
					}
					fin.close();
					fout.close();
				} else {
					// Preserve encoding
					writer = new OutputStreamWriter(fout,defaultEncoding);
					IOUtils.copy(fw.getInputStream(), writer, defaultEncoding);
					writer.close();
				}
	        } catch (IOException e) {
	        	throw new ClientException("Could not write uploaded file.");
	        }
		}
		return doProcess;
		
	}

	private boolean isZipFileName(String fileName) {
		for (String ext : zipExtensions) {
			if (fileName.toLowerCase().endsWith(ext)) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings({ "unchecked" })
	private void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		
		// Pull session times for eprime files
		Map<File,Date> datedEprime = new HashMap<File,Date>();
		List<File> eprimeFiles = getEprimeTextFiles(cacheLoc,true);
		for (File eprimeF : eprimeFiles) {
			datedEprime.put(eprimeF,getSessionTimeFromEprime(eprimeF));
		}
		// Order eprime list by time
	    List<Map<File,Date>> oDatedEprime = getDateOrderedList(datedEprime);
		
		if (oDatedEprime==null || oDatedEprime.size()<1) {
			throw new ClientException("Couldn't find a valid e-prime file in the upload.  There must be a matching \".txt\" and \".edat2\" file " +
										"and these must have matching base filenames.");
		}
	    
		// Pull scan acquisition times for tfMRI scans from DICOM
		HashMap<XnatImagescandata,Date> datedScans = new HashMap<XnatImagescandata,Date>();
		ArrayList<XnatImagescandata> scans = getBoldScans(exp);
		for (XnatImagescandata scan : scans) {
			List<XnatAbstractresourceI> cats = scan.getFile();
			for (XnatAbstractresourceI cat : cats) {
				if (cat instanceof XnatResourcecatalog && cat.getLabel().equalsIgnoreCase("DICOM")) {
					XnatResourcecatalog xcat = (XnatResourcecatalog)cat;
					ArrayList<ResourceFile> fileResList = xcat.getFileResources(proj.getRootArchivePath());
					ArrayList<File> fileList = new ArrayList<File>();
					for (ResourceFile rf : fileResList) {
						File dcmFile = new File(rf.getAbsolutePath());
						if (dcmFile.exists()) {
							fileList.add(dcmFile);
						}
					}
					Date acqTime = getAcquisitionTimeFromDicomFiles(fileList);
					datedScans.put(scan,acqTime);
				}
			}
		}
		
		// Order scan list by time
	    List<Map<XnatImagescandata,Date>> oDatedScans = getDateOrderedList(datedScans);
		
		if (oDatedScans==null || oDatedScans.size()<1) {
			throw new ClientException("Couldn't find any BOLD scans to upload e-prime files to.  Currently identifying the following scan types as BOLD scans (" +
					Arrays.toString(boldTypes) + ")");
		}
	    
	    // If fewer files than scans, remove unusable scans for matching
	    if (oDatedScans.size()>oDatedEprime.size()) {
	    	removeUnusableScans(oDatedScans);
	    }
	    if (oDatedScans.size()==oDatedEprime.size()) {
	    	// Use simple method for matching e-prime files to scans
	    	simpleMatch(oDatedEprime,oDatedScans);
	    } else if (oDatedScans.size()<oDatedEprime.size()) {
			throw new ClientException("ERROR:  Upload contains more e-prime files than BOLD scans.  Verify that upload is to the correct experiment and bold " + "" +
					"scan types are appropriately labeled (" + Arrays.toString(boldTypes) + ").  If necessary, upload data manually.");
	    } else {
	    	// Use alternate method for matching.  Perform match if all e-prime files can be matched by time to a single scan where e-prime time 
	    	// and acquisition time are within two minutes of each other
	    	diffSizeMatch(oDatedEprime,oDatedScans);
	    }
	    
	}

	// Match e-prime files to scan when number of files matches number of BOLD scans or number of usable BOLD scans
	private void simpleMatch(List<Map<File, Date>> oDatedEprime, List<Map<XnatImagescandata, Date>> oDatedScans) throws ClientException {
		boolean alreadyDiffWarn=false;
		String scan1scanNumber=null;
		Date scan1scanTime=null;
		Date scan1eprimeTime=null;
		// As an initial step, check for time inconsistencies between e-prime files and scans and warn or halt processing depending on severity.
		// NOTE:  We must assume that scanner time and e-prime computer time may not be well synchronized, but the difference between
		// scanner time and e-prime computer time between scans should be very consistent.  Issue warnings for time time differences and
		// minor inconsistencies, but halt processing for major inconsistencies between scans (>3 minutes) or minor inconsistancies (>1 minute) 
		// coupled with a shift between e-prime and scanner time for all scans.
		for (int i=0;i<oDatedScans.size();i++) {
			
			Map<XnatImagescandata,Date> sMap = oDatedScans.get(i);
			Map<File,Date> eMap = oDatedEprime.get(i);
			
			Map.Entry<XnatImagescandata,Date> sEntry = sMap.entrySet().iterator().next();
			Map.Entry<File,Date> eEntry =  eMap.entrySet().iterator().next();
			XnatImagescandata thisScan = sEntry.getKey();
			Date scanTime = sEntry.getValue();
			Date eprimeTime = eEntry.getValue();
			
			if (i==0) {
				scan1scanNumber=thisScan.getId();
				scan1scanTime=scanTime;
				scan1eprimeTime=eprimeTime;
			}
			
			// Warn if acquisition dates differ by more than 5 minutes
			if (scanTime!=null && eprimeTime!=null) {
				// Make sure differences between scanner time and e-prime time are similar between scans otherwise halt processing or warn
				if (Math.abs((scan1scanTime.getTime()-scan1eprimeTime.getTime())-(scanTime.getTime()-eprimeTime.getTime()))>(1000*60*3)) {
					throw new ClientException("ERROR:  Time difference between scanner and e-prime computer time differs between scans by more than three minutes.  " +
								"The time difference between scans should be very consistent.  Please check the data and upload manually if necessary.  " +
							    "Scan " + scan1scanNumber + " " +
								"(scanTime=" + scan1scanTime + ", eprimeTime=" + scan1eprimeTime + ", difference=" + ((scan1scanTime.getTime()-scan1eprimeTime.getTime())/1000) +
								" Sec.  Scan " + thisScan.getId() + 
								" (scanTime=" + scanTime + ", eprimeTime=" + eprimeTime + ", difference=" + ((scanTime.getTime()-eprimeTime.getTime())/1000) + " Sec.)" );
				}
				if (Math.abs((scan1scanTime.getTime()-scan1eprimeTime.getTime())-(scanTime.getTime()-eprimeTime.getTime()))>(1000*60)) {
					returnList.add("WARNING:  Time difference between scanner and e-prime computer time differs between scans by more than one minute.  " +
							    "This should be checked. Scan " + scan1scanNumber + " " +
								"(scanTime=" + scan1scanTime + ", eprimeTime=" + scan1eprimeTime + ", difference=" + ((scan1scanTime.getTime()-scan1eprimeTime.getTime())/1000) +
								" Sec.  Scan " + thisScan.getId() + 
								" (scanTime=" + scanTime + ", eprimeTime=" + eprimeTime + ", difference=" + ((scanTime.getTime()-eprimeTime.getTime())/1000) + " Sec.)" );
				}
				// Issue warning if time difference between scanner and e-prime time is greater than 5 minutes
				if (!alreadyDiffWarn && Math.abs(sEntry.getValue().getTime()-eEntry.getValue().getTime())>(1000*60*5)) {
					returnList.add("WARNING:  acquisition time differs between DICOM and EPRIME one or more scans by more than 5 minutes " +
							"(" + scanTime + " vs. " + eprimeTime + ").  " +
							"This could be due to clock differences between the scanner and e-prime computer, but should be checked.");
					alreadyDiffWarn=true;
				}
				if (Math.abs(sEntry.getValue().getTime()-eEntry.getValue().getTime())>(1000*60*5) && 
						(Math.abs((scan1scanTime.getTime()-scan1eprimeTime.getTime())-(scanTime.getTime()-eprimeTime.getTime()))>(1000*60))) {
					throw new ClientException("ERROR:  Acquisition time differs between DICOM and EPRIME time in one or more scans by more than 5 minutes AND " + 
												"time difference between scanner time and acquisition time differs between scans.  " +
												"Please verify upload is to the correct experiment.  Upload manually if necessary");
				}
			} else {
				if (scanTime==null) {
					throw new ClientException("ERROR:  DICOM acquisition time could not be determined for one or more scans.  E-prime could not be matched to scans.  " + 	
												"Check the data and upload manually if necessary");
				}
				throw new ClientException("ERROR:  E-prime acquisition time could not be determined from one or more imported files.  E-prime could not be matched to scans.  " + 	
											"Check the data and upload manually if necessary");
			}
			
		}
		// Upload as a separate step from above to prevent partial upload upon exception 
		for (int i=0;i<oDatedScans.size();i++) {
			
			Map<XnatImagescandata,Date> sMap = oDatedScans.get(i);
			Map<File,Date> eMap = oDatedEprime.get(i);
			
			Map.Entry<XnatImagescandata,Date> sEntry = sMap.entrySet().iterator().next();
			Map.Entry<File,Date> eEntry =  eMap.entrySet().iterator().next();
			XnatImagescandata thisScan = sEntry.getKey();
			Date scanTime = sEntry.getValue();
			File eprimeFile = eEntry.getKey();
			Date eprimeTime = eEntry.getValue();
			
			// Upload e-prime files as scan resource
			uploadEprimeFileToScan(thisScan,scanTime,eprimeFile,eprimeTime);
			
		}
		
	}
	
	// Attempt match when fewer e-prime files than BOLD scans.  Requires closely calibrated scanner/eprime computer time.  Look for
	// cases where each given e-prime file matches to one and only one scan (attempt match allowing time variations of one minute,
	// two minutes, then three minutes.
	private void diffSizeMatch(List<Map<File, Date>> oDatedEprime, List<Map<XnatImagescandata, Date>> oDatedScans) throws ClientException {
		
		List<Map<XnatImagescandata,File>> matches = new ArrayList<Map<XnatImagescandata,File>>();
		boolean toomany=false;
		Map<XnatImagescandata,File> matchMap = new HashMap<XnatImagescandata,File>();
		// Loop over all e-prime files, add match if there is only one match where e-prime time and DICOM acquisition differ by less than X minutes
		for (int m=1;m<=3;m++) {
			// skip if already matched or scan has already matched to multiple e-prime files
			if (toomany || matches.size()==oDatedEprime.size()) {
				continue;
			}
			for (int i=0;i<oDatedScans.size();i++) {
				Map<XnatImagescandata,Date> sMap = oDatedScans.get(i);
				Map.Entry<XnatImagescandata,Date> sEntry = sMap.entrySet().iterator().next();
				XnatImagescandata thisScan = sEntry.getKey();
				Date scanTime = sEntry.getValue();
				
				boolean already=false;
				for (Map<File,Date> eMap : oDatedEprime) {
					Map.Entry<File,Date> eEntry =  eMap.entrySet().iterator().next();
					File eprimeFile = eEntry.getKey();
					Date eprimeTime = eEntry.getValue();
					if (scanTime!=null && eprimeTime!=null) {
						if (!toomany && !already && Math.abs(sEntry.getValue().getTime()-eEntry.getValue().getTime())<(1000*60*m)) {
							matchMap.put(thisScan,eprimeFile);
							already=true;
						} else if (already && Math.abs(sEntry.getValue().getTime()-eEntry.getValue().getTime())<(1000*60*m)) {
							toomany=true;
							break;
						}
					}
				}
			}
		}
		// If there's a single match for each e-prime file, proceed with upload
		if (matchMap.entrySet().size()==oDatedEprime.size()) {
			for (Map.Entry<XnatImagescandata,File> mEntry : matchMap.entrySet()) {
				XnatImagescandata imageScan = mEntry.getKey();
				File eprimeFile = mEntry.getValue();
				if (imageScan!=null && eprimeFile!=null) {
					// Upload e-prime files as scan resource
					uploadEprimeFileToScan(imageScan,getTime(oDatedScans,imageScan),eprimeFile,getTime(oDatedEprime,eprimeFile));
				}
			}
			return;
		}
		// Otherwise, throw ClientError exception
		throw new ClientException("ERROR:  Mismatch between number of (usable) scans and e-prime files,  and the e-prime files could not be matched reliably to existing " +
									"scans.  This could be due to a lack of synchronization between scanner time and e-prime computer time. However, please check " +
									"that upload is to correct experiment.  If necessary,upload data manually");
	}
	
	private Date getTime(List<Map<File, Date>> oDatedEprime, File eprimeFile) {
		for (Map<File,Date> map : oDatedEprime) {
			Map.Entry<File,Date> entry =  map.entrySet().iterator().next();
			if (entry.getKey().equals(eprimeFile)) {
				return entry.getValue();
			}
		}
		return null;
	}

	private Date getTime(List<Map<XnatImagescandata, Date>> oDatedScans, XnatImagescandata imageScan) {
		for (Map<XnatImagescandata,Date> map : oDatedScans) {
			Map.Entry<XnatImagescandata,Date> entry =  map.entrySet().iterator().next();
			if (entry.getKey().equals(imageScan)) {
				return entry.getValue();
			}
		}
		return null;
	}

	private void uploadEprimeFileToScan(XnatImagescandata thisScan, Date scanTime, File eprimeFile, Date eprimeTime) throws ClientException {
		
		if (scanTime==null || eprimeTime==null) {
			throw new ClientException("ERROR:  could not find eprime files");
		}
		File scanDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + exp.getLabel() +
					File.separator + "SCANS" + File.separator + thisScan.getId() + File.separator + "EPRIME");
		if (!scanDir.exists()) {
			scanDir.mkdir();
		}
		File catFile = new File(scanDir,"eprime_" + thisScan.getId() + "_catalog.xml");
		
		// Create catalog of files
		CatCatalog cat = new CatCatalog();
		// copy text and edat2 file to archive catalog location
		File eprimeTextFile = eprimeFile;
		File eprimeEdatFile = new File(eprimeTextFile.getParentFile(),eprimeTextFile.getName().substring(0,eprimeTextFile.getName().length()-3) + "edat2");
		
		if (!(eprimeTextFile.exists() && eprimeEdatFile.exists())) {
			throw new ClientException("ERROR:  could not find eprime files");
		}
		File outTextFile = new File(scanDir,eprimeTextFile.getName());
		File outEdatFile = new File(scanDir,eprimeEdatFile.getName());
		try {
			FileUtils.copyFile(eprimeTextFile,outTextFile);
			CatEntry textEntry = new CatEntry();
			textEntry.setName(outTextFile.getName());
			textEntry.setUri(outTextFile.getName());
			cat.addEntries_entry(textEntry);
			FileUtils.copyFile(eprimeEdatFile,outEdatFile);
			CatEntry edatEntry = new CatEntry();
			edatEntry.setName(outEdatFile.getName());
			edatEntry.setUri(outEdatFile.getName());
			cat.addEntries_entry(edatEntry);
		} catch (Exception e) {
			throw new ClientException(e.getMessage(),new Exception());
		}
		
		// Create Eprime Resource
		XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		}
		ecat.setLabel("EPRIME");
		ecat.setFormat("EPRIME");
		ecat.setContent("RAW");
		// Save resource to scan
		try {
			thisScan.addFile(ecat);
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, thisScan.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.MODIFY_VIA_WEB_SERVICE, EVENT_REASON, null));
			final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(thisScan,user,false,true,ci)) {
				PersistentWorkflowUtils.complete(wrk, ci);
			} else {
				PersistentWorkflowUtils.fail(wrk,ci);
			}

			returnList.add("Eprime file " + eprimeFile.getName() + " successfully uploaded to eprime resource under scan " + thisScan.getId()); 
		} catch (Exception e) {
			throw new ClientException("ERROR:  Couldn't add resource to scan - " + e.getMessage(),new Exception());
		}
		
	}

	private void removeUnusableScans(List<Map<XnatImagescandata,Date>> oDatedScans) {
		// Using iterator here because removing within for/next can be unpredictable
		Iterator<Map<XnatImagescandata,Date>> sIter = oDatedScans.iterator();
		while (sIter.hasNext()) {
			Map<XnatImagescandata,Date> eMap = sIter.next();
			XnatImagescandata scan = eMap.keySet().iterator().next();
			if (scan.getQuality().equalsIgnoreCase("unusable")) {
				sIter.remove();
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List getDateOrderedList(Map inMap) {
		LinkedList<Map.Entry<Object,Date>> list = new LinkedList(inMap.entrySet());
	    Collections.sort(list, new Comparator() {
	          public int compare(Object o1, Object o2) {
	               if (((Date)((Map.Entry)(o1)).getValue()).before((Date)((Map.Entry)(o2)).getValue())) {
	            	   return -1;
	               } else if (((Date)((Map.Entry)(o1)).getValue()).equals((Date)((Map.Entry)(o2)).getValue())) {
	            	   return 0;
	               } else {
	            	   return 1;
	               }
	          }
	    });
	    List<Map<Object,Date>> rList = new ArrayList<Map<Object,Date>>();
	    for (Map.Entry<Object,Date>entry : list) {
	        HashMap<Object,Date> tMap = new HashMap<Object,Date>();
	        tMap.put(entry.getKey(), entry.getValue());
	        rList.add(tMap);
	    }
	    return rList;
	}

	private Date getSessionTimeFromEprime(File eprimeF) {
		try {
			 String dateStr = null;
			 String timeStr = null;
			 // NOTE:  E-prime text files seem usually to be stored in UTF-16le format.  Checking format here
			 // in case someone modifies it. 
			 String fileEncoding = getFileEncoding(eprimeF);
			 LineIterator it = FileUtils.lineIterator(eprimeF, fileEncoding);
			 try {
				 while (it.hasNext()) {
					 String lineStr = it.nextLine();
					 if (lineStr!=null && lineStr.length()>12 && lineStr.substring(0,12).equalsIgnoreCase("SessionDate:")) {
						 dateStr=lineStr.substring(13).trim();
					 } else if (lineStr!=null && lineStr.length()>12 && lineStr.substring(0,12).equalsIgnoreCase("SessionTime:")) {
						 timeStr=lineStr.substring(13).trim();
						 break;
					 }
				 }
			} finally {
				 LineIterator.closeQuietly(it);
			}
			if (dateStr!=null && timeStr!=null) {
				return (new SimpleDateFormat("MM-dd-yyyy hh:mm:ss")).parse(dateStr + " " + timeStr);
			}
			return null;
		} catch (Exception e){
			return null;
		}
	}

	private String getFileEncoding(File eprimeF) {
		
		
	    byte[] buf = new byte[4096];
	    java.io.FileInputStream fis;
		try {
			fis = new java.io.FileInputStream(eprimeF);
		} catch (FileNotFoundException e) {
			return defaultEncoding;
		}

	    UniversalDetector detector = new UniversalDetector(null);

	    int nread;
	    try {
			while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
			  detector.handleData(buf, 0, nread);
			}
		} catch (IOException e) {
			return defaultEncoding;
		}
	    try {
			fis.close();
		} catch (IOException e) {
			logger.error(e);
		}
	    detector.dataEnd();

	    String encoding = detector.getDetectedCharset();
	    if (encoding == null) {
	    	encoding = defaultEncoding;
	    }
	    
	    detector.reset();
		return encoding;
		
	}

	private Date getAcquisitionTimeFromDicomFiles(ArrayList<File> fileList) {
		Date acqDateTime = null;
		for (File file : fileList) {
			try {
				if (file.getName().toLowerCase().endsWith(dcmExtension)) {
					DicomObject dcmObj = DicomUtils.read(file, Tag.AcquisitionTime);
					Date thisDateTime = dcmObj.getDate(Tag.AcquisitionDateTime);
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.AcquisitionDate,Tag.AcquisitionTime);
					}
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.SeriesDate,Tag.AcquisitionTime);
					}
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.StudyDate,Tag.AcquisitionTime);
					}
					if (thisDateTime!=null && (acqDateTime==null || thisDateTime.before(acqDateTime))) {
						acqDateTime=thisDateTime;
					}
				}
			} catch (IOException e) {
				// Do nothing for now
			}
		}
		return acqDateTime;
	}

	private Date getDateTimeByTags(DicomObject dcmObj, int dteval, int tmeval) {
		Date thisDate = dcmObj.getDate(dteval);
		Date thisTime = dcmObj.getDate(tmeval);
		if (thisDate!=null && thisTime!=null) {
			Calendar cal = Calendar.getInstance();
			Calendar dCal = Calendar.getInstance();
			cal.setTime(thisTime);
			dCal.setTime(thisDate);
			cal.set(dCal.get(Calendar.YEAR),dCal.get(Calendar.MONTH),dCal.get(Calendar.DAY_OF_MONTH));
			return cal.getTime(); 
		}
		return null;
	}

	private ArrayList<XnatImagescandata> getBoldScans(XnatMrsessiondata exp) {
		ArrayList<XnatImagescandata> boldList = new ArrayList<XnatImagescandata>();
		for (String scanType : boldTypes) {
			boldList.addAll(exp.getScansByType(scanType));
		}
		return boldList;
	}

	public List<File> getEprimeTextFiles(File dir,boolean recursive) {
		final List<File> files=new ArrayList<File>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getEprimeTextFiles(f,true));
				
			} else {
				if (f.getName().toLowerCase().endsWith(".txt")) {
					// Must have matching .edat2 file in same directory
					File edatf = new File(f.getParentFile(),f.getName().substring(0,f.getName().length()-3) + "edat2");
					if (edatf.exists()) {
						files.add(f);
					} else {
						returnList.add("WARNING:  E-prime text file (" + f.getName() + ") has no matching edat2 file.");
					}
				}
			}
		}
		return files;
	}
	
	private ZipI getZipper(String fileName) {
		
		// Assume file name represents correct compression method
        String file_extension = null;
        if (fileName!=null && fileName.indexOf(".")!=-1) {
        	file_extension = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        	if (Arrays.asList(zipExtensions).contains(file_extension)) {
        		return new ZipUtils();
	        } else if (file_extension.equalsIgnoreCase(".tar")) {
        		return new TarUtils();
	        } else if (file_extension.equalsIgnoreCase(".gz")) {
	        	TarUtils zipper = new TarUtils();
	        	zipper.setCompressionMethod(ZipOutputStream.DEFLATED);
	        	return zipper;
	        }
        }
        // Assume zip-compression for unnamed inbody files
        return new ZipUtils();
        
	}
	
}

