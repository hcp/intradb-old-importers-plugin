package org.nrg.hcp.importer.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.attr.Utils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.bean.CatEntryBean;
import org.nrg.xdat.bean.XnatMegscandataBean;
import org.nrg.xdat.bean.XnatMegsessiondataBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMegsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.BaseXnatResourcecatalog;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXReader;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.nrg.xnat.services.archive.CatalogService;

public class HCPMegRepresentation {
	
	static Logger logger = Logger.getLogger(HCPMegRepresentation.class);

	private String patientID; 
	private String subjectLbl; 
	private String expLbl; 
	private File megDirectory; 
	private File tocFile; 
	private UserI user;
    private XnatProjectdata proj;
    private Map<File,File> eprimeMap;
    private final Map<String,TreeSet<File>> eprimeTypeMap = new HashMap<String,TreeSet<File>>();
    private final Map<String,TreeSet<XnatMegscandataBean>> scanTypeMap = new HashMap<String,TreeSet<XnatMegscandataBean>>();
    private final Map<String,TreeSet<XnatMegscandataBean>> usableScanTypeMap = new HashMap<String,TreeSet<XnatMegscandataBean>>();
    private Map<String,Integer> sdCountMap = null;
    private List<File> eegFiles;
	private List<String> returnList = new ArrayList<String>();
	
	private ArrayList<MEGScan> scanList; 
	private SortedMap<File,SortedMap<File,ArrayList<File>>> dirStructure;
	
	private static final String DEFAULT_EXT = "_MEG";
	private static final String[] NOISE_FILES = { "c,rfDC","config" };
	private static final String[] MEG_FILES = { "c,rfDC","hs_file","config","e,rfhp[0-9.]*Hz,COH","e,rfhp[0-9.]*Hz,COH1" };
    private static final Map<String,String> convertMap;
    //private static final Map<String,Map<String,Long[]>> fileSizeMap;
	//private static final DateFormat DDT_DF = new SimpleDateFormat("MM%dd%yy@HH_mm"); 
	private static final DateFormat DDT_DF = new SimpleDateFormat("MM%dd%yy%HH%mm"); 
	private static final String DDT_SUBST = "\\D"; 
	private static final DateFormat SDA_DF = new SimpleDateFormat("MM/dd/yy HH:mm"); 
	private static final DateFormat STD_DF = new SimpleDateFormat("MM%dd%yy"); 
	private static final Pattern EPRIME_DP = Pattern.compile("_\\d\\d-\\d\\d-\\d\\d\\d\\d_\\d\\d.\\d\\d.\\d\\d"); 
	private static final DateFormat EPRIME_DF = new SimpleDateFormat("MM-dd-yyyy_HH.mm.ss");
	private static final SimpleDateFormat TIME_FMT = new SimpleDateFormat("HH:mm:ss");
	private static enum TASK_TYPES { WM,MOTOR,SENT,STORY };
	private static final String[] NOISE_STRINGS = { "Rnoise","Pnoise" };
	private static final String[] TASK_STRINGS = { "Wrkmem","Motort","Sentnc","StoryM" };
	private static final String[] OTHER_STRINGS = { "Restin", "Biocal" };
	static final String NO_EPRIME_STR = "Uploader found no e-prime files for this scan";

	// Per e-mail from Abbas, only series descriptions with "_B" should have EEG files attached
	private static final String EEG_INCLUDE = "_B";
    static {
    	
        final Map<String,String> tmap = new HashMap<String,String>();
        tmap.put("RNois","Rnoise");
        tmap.put("PNois","Pnoise");
        tmap.put("BioCl","Biocal");
        tmap.put("Rest","Restin");
        tmap.put("WkMem","Wrkmem");
        tmap.put("Motor","Motort");
        tmap.put("Sentn","Sentnc");
        tmap.put("Story","StoryM");
        convertMap = Collections.unmodifiableMap(tmap);
        
	
        // Comment out this section for now.  Per 2013/08/07 e-mail, Linda is okay with not checking file size.  
		/*
        final Map<String,Map<String,Long[]>> scanfsmap = new HashMap<String,Map<String,Long[]>>();
        final Map<String,Long[]> sm1 = new HashMap<String,Long[]>();
        sm1.put("c,rfDC",new Long[] {416361000L,1665444000L});
        //sm1.put("c,rfDC",new Long[] {5L,1665444000L});
        sm1.put("config",new Long[] {180000L,200000L});
        scanfsmap.put("Rnoise", sm1);
        final Map<String,Long[]> sm2 = new HashMap<String,Long[]>();
        sm2.put("c,rfDC",new Long[] {83272200L,333088800L});
        //sm2.put("c,rfDC",new Long[] {5L,333088800L});
        sm2.put("config",new Long[] {180000L,200000L});
        scanfsmap.put("Pnoise", sm2);
        final Map<String,Long[]> sm3 = new HashMap<String,Long[]>();
        sm3.put("c,rfDC",new Long[] {666177600L,2664710400L});
        //sm3.put("c,rfDC",new Long[] {5L,2664710400L});
        sm3.put("config",new Long[] {180000L,200000L});
        sm3.put("e,rfhp1.0Hz,COH",new Long[] {997600L,1997600L});
        sm3.put("e,rfhp1.0Hz,COH1",new Long[] {1997600L,1997600L});
        sm3.put("hs_file",new Long[] {40000L,150000L});
        scanfsmap.put("Restin", sm3);
        final Map<String,Long[]> sm4 = new HashMap<String,Long[]>();
        sm4.put("c,rfDC",new Long[] {832722000L,3330888000L});
        //sm4.put("c,rfDC",new Long[] {5L,3330888000L});
        sm4.put("config",new Long[] {180000L,200000L});
        sm4.put("e,rfhp1.0Hz,COH",new Long[] {1997600L,1997600L});
        sm4.put("e,rfhp1.0Hz,COH1",new Long[] {1997600L,1997600L});
        sm4.put("hs_file",new Long[] {40000L,150000L});
        scanfsmap.put("Wrkmem", sm4);
        final Map<String,Long[]> sm5 = new HashMap<String,Long[]>();
        sm5.put("c,rfDC",new Long[] {1082538600L,4330154400L});
        //sm5.put("c,rfDC",new Long[] {5L,4330154400L});
        sm5.put("config",new Long[] {180000L,200000L});
        sm5.put("e,rfhp1.0Hz,COH",new Long[] {1997600L,1997600L});
        sm5.put("e,rfhp1.0Hz,COH1",new Long[] {1997600L,1997600L});
        sm5.put("hs_file",new Long[] {40000L,150000L});
        scanfsmap.put("Motort", sm5);
        final Map<String,Long[]> sm6 = new HashMap<String,Long[]>();
        sm6.put("c,rfDC",new Long[] {666177600L,2664710400L});
        //sm6.put("c,rfDC",new Long[] {5L,2664710400L});
        sm6.put("config",new Long[] {180000L,200000L});
        sm6.put("e,rfhp1.0Hz,COH",new Long[] {1997600L,1997600L});
        sm6.put("e,rfhp1.0Hz,COH1",new Long[] {1997600L,1997600L});
        sm6.put("hs_file",new Long[] {40000L,150000L});
        scanfsmap.put("StoryM", sm6);
        final Map<String,Long[]> sm7= new HashMap<String,Long[]>();
        sm7.put("c,rfDC",new Long[] {915994200L,3663976800L});
        //sm7.put("c,rfDC",new Long[] {5L,3663976800L});
        sm7.put("config",new Long[] {180000L,200000L});
        sm7.put("e,rfhp1.0Hz,COH",new Long[] {1997600L,1997600L});
        sm7.put("e,rfhp1.0Hz,COH1",new Long[] {1997600L,1997600L});
        sm7.put("hs_file",new Long[] {40000L,150000L});
        scanfsmap.put("Sentnc", sm7);
        fileSizeMap = Collections.unmodifiableMap(scanfsmap);
		*/
    }
    
	private static final Comparator<File> eprimeFileCompare = new Comparator<File>() {
		@Override
		public int compare(File arg0, File arg1) {
			final Date d0 = getDateFromEprimeFile(arg0);
			final Date d1 = getDateFromEprimeFile(arg1);
			if (d0!=null && d1!=null && !d0.equals(d1)) {
				return d0.compareTo(d1);
			} else {
				return arg0.getName().compareTo(arg1.getName());
			}
		}
	};
	
	private static final Comparator<XnatMegscandataBean> scanCompare = new Comparator<XnatMegscandataBean>() {
		@Override
		public int compare(XnatMegscandataBean arg0, XnatMegscandataBean arg1) {
			try {
				return Float.valueOf(arg0.getId()).compareTo(Float.valueOf(arg1.getId()));
			} catch (NumberFormatException e) {
				final String numPart0 = arg0.getId().replaceAll("\\D", "");
				final String numPart1 = arg1.getId().replaceAll("\\D", "");
				if (!numPart0.equals(numPart1)) {
					return numPart0.compareTo(numPart1);
				}
				return arg0.getId().compareTo(arg1.getId());
			}
		}
	};

	private static Date getDateFromEprimeFile(File zipFile) {
		final Matcher matcher = EPRIME_DP.matcher(zipFile.getName());
		if (matcher.find()) {
			try {
				return EPRIME_DF.parse(matcher.group().substring(1));
			} catch (ParseException e1) { }
		}
		return null;
	}
	
	/*
	 * Create MEG Representation from tocf file contained in the archive.  Builds archive session from the representation.
	 * @author Mike Hodge <hodgem@mir.wustl.edu>
	 */
	public HCPMegRepresentation(File tocf) throws ClientException {
		
		try {
			returnList.add("NOTE:  Upload is in older (tocf.txt) format");
			final BufferedReader reader = new BufferedReader(new FileReader(tocf));
			this.tocFile = tocf;
			try {
				String line;
				boolean insideRun = false;
				int pdfCount = 0;
				MEGSession currentSession=null;
				MEGScan currentScan=null;
				MEGRun currentRun=null;
				
				// The first line contains exeriment label
				this.expLbl = reader.readLine();
				this.megDirectory = tocf.getParentFile();
				
				while ((line = reader.readLine()) != null) {
					String[] lineComps = line.split("	");
					
					if (lineComps.length>=4 && lineComps[0].equals("PATIENT")) {
						
						this.patientID = dequote(lineComps[2]);
						this.subjectLbl = dequote(lineComps[3]);
						this.scanList = new ArrayList<MEGScan>();
						
					} else if (lineComps.length>=4 && lineComps[1].equals("SCAN")) {
						
						final MEGScan megScan = new MEGScan();
						megScan.setScanID(dequote(lineComps[2]));
						megScan.setScanLbl(dequote(lineComps[3]));
						megScan.setSessionList(new ArrayList<MEGSession>());
						scanList.add(megScan);
						
					} else if (lineComps.length>=5 && lineComps[2].equals("SESSION")) {
						
						final MEGSession  megSession = new MEGSession();
						megSession.setSessionID(dequote(lineComps[3]));
						megSession.setSessionDateStr(dequote(lineComps[4]));
						megSession.setRunList(new ArrayList<MEGRun>());
						currentScan = scanList.get(scanList.size()-1);
						currentScan.getSessionList().add(megSession);
						
					} else if (lineComps.length>=6 && lineComps[3].equals("RUN")) {
						
						insideRun = true;
						pdfCount = 0;
						final MEGRun megRun = new MEGRun();
						megRun.setRunID(dequote(lineComps[4]));
						megRun.setRunLbl(dequote(lineComps[5]));
						megRun.setFileList(new ArrayList<MEGFile>());
						currentSession = currentScan.getSessionList().get(currentScan.getSessionList().size()-1); 
						currentSession.getRunList().add(megRun);
						
					} else if (lineComps.length>=4 && lineComps[3].equals("/RUN")) {
						
						insideRun = false;
						
					} else if (insideRun) {
						
						final MEGFile megFile = new MEGFile();
						megFile.setFileType(dequote(lineComps[4]));
						if (megFile.getFileType().equals("CONFIG")) {
							megFile.setFileIn("config");
							megFile.setFileOut("config");
						} else if (megFile.getFileType().equals("HS_FILE")) {
							megFile.setFileIn("hs_file");
							megFile.setFileOut("hs_file");
						} else if (megFile.getFileType().equals("PDF")) {
							// Note:  filename starts from 0, so set this pre-increment
							megFile.setFileIn(Integer.toString(pdfCount));
							megFile.setFileOut(dequote(lineComps[5]));
							pdfCount++;
						} else {
							throw new ClientException("ERROR:  Unexpected tocf file type (" + megFile.getFileType() + ")",new Exception());
						}
						currentRun = currentSession.getRunList().get(currentSession.getRunList().size()-1); 
						currentRun.getFileList().add(megFile);
						
					}
					
				}
				reader.close();
				// re-order sessions within scans by date, then order scans by containing session date
				for (final MEGScan scan : scanList) {
					Collections.sort(scan.getSessionList());
				}
				Collections.sort(scanList);
				
			} catch (IOException e) {
				throw new ClientException(e.getMessage(),new Exception());
			} finally {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) {
			throw new ClientException(e.getMessage(),new Exception());
		}
		
	}
	
	/*
	 * Create MEG Representation from newer format (no tocf file).  Builds archive session from the representation.
	 * @author Mike Hodge <hodgem@mir.wustl.edu>
	 */
	public HCPMegRepresentation(UserI user, XnatProjectdata proj,final String cachepath) throws ClientException {
		returnList.add("NOTE:  Upload is in newer (non-tocf.txt) format");
		final ArrayList<File> zipList = getFileListFromDir(new File(cachepath));
		this.proj = proj;
		this.user = user;
		this.dirStructure = getMegDirStructure(zipList);
		this.eprimeMap = getEprimeMap(zipList);
		this.eegFiles = getEEGFiles(zipList);
	}
	
	private List<File> getEEGFiles(List<File> zipList) {
		final List<File> fileList = new ArrayList<File>();
		// Per Abbas, 2013/04/15, currently not appending EEG files.  Leaving code in place in case decision is reversed or revised
		/*
		while (zipI.hasNext()) {
			final File zipFile = zipI.next();
			if (zipFile.getName().endsWith(".el") || zipFile.getName().endsWith("hs") || zipFile.getName().endsWith("el.ascii")) {
				for (final File f : zipFile.getParentFile().listFiles()) {
					if (f.isFile() && f.exists()) {
						if (!(f.getName().endsWith(".el") || f.getName().endsWith("hs") || f.getName().endsWith("el.ascii"))) {
							returnList.add("WARNING:  EEG directory includes unexpected file (" + f.getName() + ").  This file will be ignored.");
						}
						// Per e-mail from RO on 2013/03/16 - rename these files  
						String newName = f.getName().replaceFirst("^[^.]+[.]", expLbl + ".");
						if (!newName.equals(f.getName())) {
							File newFile = new File(f.getParentFile(),newName);
							if (f.renameTo(newFile)) {
								returnList.add("NOTE:  EEG file " + f.getName() + " was renamed to " + newFile.getName());
								fileList.add(newFile);
								continue;
							}
						}
						fileList.add(f);
					}
				}
				break;
			}
		}
		*/
		return fileList;
	}

	private Map<File, File> getEprimeMap(ArrayList<File> zipList) {
		final Iterator<File> zipI = zipList.iterator();
		final Map<File,File> returnMap = new HashMap<File,File>();
		while (zipI.hasNext()) {
			final File zipFile = zipI.next();
			if (zipFile.getName().endsWith(".edat2") && !
					(zipFile.getName().toLowerCase().contains("practice") || zipFile.getName().toLowerCase().contains("runp"))) {
				// Find and place matching txt file
				final String tPath = zipFile.getAbsolutePath().substring(0,zipFile.getAbsolutePath().lastIndexOf(".edat2")) + ".txt";
				final File tFile = new File(tPath);
				if (zipList.contains(tFile)) {
					final File epFile = renameEprimeFile(zipFile);
					final File tpFile = renameEprimeFile(tFile);
					returnMap.put(epFile,tpFile);
					populateEprimeTypeMap(epFile);
				}
			} else if (zipFile.getName().endsWith(".edat2")) {
				returnList.add("NOTE:  e-prime file " + zipFile.getName() + " and its associated txt file appear to be practice files.  They will not be uploaded.");
				zipI.remove();
			}
		}
		return returnMap;
	}

	private File renameEprimeFile(File f) {
		String newName = f.getName().replaceFirst("__.*[.]",".");
		final String[] parts = newName.split("_");
		if (parts.length>=2) {
			if (parts[1].toUpperCase().startsWith("MEG")) {
				newName = newName.replaceFirst("^[^_]*_[^_]*_", expLbl + "_");
			} else {
				newName = newName.replaceFirst("^[^_]*_", expLbl + "_");
			}
		}
		if (!newName.equals(f.getName())) {
			final File newFile = new File(f.getParentFile(),newName);
			if (f.renameTo(newFile)) {
				returnList.add("NOTE:  EPRIME file " + f.getName() + " was renamed to " + newFile.getName());
				return newFile;
			}
		}
		return f;
	}

	private void populateEprimeTypeMap(File zipFile) {
		String ftype;
		final String fn = zipFile.getName();
		if (fn.toLowerCase().contains("_motor")) {
			ftype = TASK_TYPES.MOTOR.toString(); 
		} else if (fn.toLowerCase().contains("_sent")) {
			ftype = TASK_TYPES.SENT.toString(); 
		} else if (fn.toLowerCase().contains("_story")) {
			ftype = TASK_TYPES.STORY.toString(); 
		} else if (fn.toLowerCase().contains("_wm") || fn.toLowerCase().contains("_wrkmem")) {
			ftype = TASK_TYPES.WM.toString(); 
		} else {
			return;
		}
		if (!eprimeTypeMap.containsKey(ftype)) {
			final TreeSet<File> fset = new TreeSet<File>(eprimeFileCompare);
			fset.add(zipFile);
			eprimeTypeMap.put(ftype,fset);
		} else {
			eprimeTypeMap.get(ftype).add(zipFile);			
		}
	}

	private void populateScanTypeMaps(XnatMegscandataBean scan) {
		String stype;
		if (scan.getSeriesDescription().contains("Motor")) {
			stype = TASK_TYPES.MOTOR.toString(); 
		} else if (scan.getSeriesDescription().contains("Sentnc")) {
			stype = TASK_TYPES.SENT.toString(); 
		} else if (scan.getSeriesDescription().contains("Story")) {
			stype = TASK_TYPES.STORY.toString(); 
		} else if (scan.getSeriesDescription().contains("Wrkmem")) {
			stype = TASK_TYPES.WM.toString(); 
		} else {
			return;
		}
		addScanToScanTypeMap(scan,stype,scanTypeMap);
		if (scan.getQuality().equals("unusable")) {
			return;
		}
		addScanToScanTypeMap(scan,stype,usableScanTypeMap);
	}

	private void addScanToScanTypeMap(XnatMegscandataBean scan, String stype, Map<String, TreeSet<XnatMegscandataBean>> imap) {
		if (!imap.containsKey(stype)) {
			final TreeSet<XnatMegscandataBean> sset = new TreeSet<XnatMegscandataBean>(scanCompare);
			sset.add(scan);
			imap.put(stype,sset);
		} else {
			imap.get(stype).add(scan);			
		}
		
	}

	private SortedMap<File, SortedMap<File, ArrayList<File>>> getMegDirStructure(final ArrayList<File> zipList) throws ClientException {
		
		this.subjectLbl = null;
		
		final Comparator<File> zipFileCompare = new Comparator<File>() {
			@Override
			public int compare(File arg0, File arg1) {
				// Some comparisons will be of the same directory
				if (arg0 == arg1 || arg0.equals(arg1)) {
					return 0;
				}
				// directory beneath should be date named according to date and time
				if (arg0.list().length==1 && arg1.list().length==1) {
					final Date d0 = dirToDateTime(arg0.listFiles()[0]);
					final Date d1 = dirToDateTime(arg1.listFiles()[0]);
					if (d0!=null && d1!=null) {
						return d0.compareTo(d1);
					}
				}
				return arg0.getName().compareTo(arg1.getName());
			}
		};
		
		final SortedMap<File, SortedMap<File,ArrayList<File>>> returnMap = new TreeMap<File,SortedMap<File,ArrayList<File>>>(zipFileCompare);
		
		final Iterator<File> zipI = zipList.iterator();
		while (zipI.hasNext()) {	
			final File zipFile=zipI.next();
			if (isMegFile(zipFile)) {
				final File megParent = zipFile.getParentFile();
				if (megParent==null) { 
					throw new ClientException("MEG File found in unexpected directory location (No parent directory)");
				}
				final File megGGParent = (megParent.getParentFile()!=null) ? megParent.getParentFile().getParentFile() : null;
				if (megGGParent==null) { 
					throw new ClientException("MEG File found in unexpected directory location (No GG parent directory)");
				}
				if (megGGParent.getParentFile()==null) { 
					throw new ClientException("MEG File found in unexpected directory location (MEG files should reside under a directory beginning with the subject label)");
				}
				if (this.subjectLbl == null) {
					this.subjectLbl = megGGParent.getParentFile().getName().replaceFirst("_.*$","");
					this.expLbl = newExpLbl();
				}
				if (!returnMap.keySet().contains(megGGParent)) {
					final SortedMap<File,ArrayList<File>> parentMap = new TreeMap<File,ArrayList<File>>(zipFileCompare);
					final ArrayList<File> megList = new ArrayList<File>();
					megList.add(zipFile);
					zipI.remove();
					parentMap.put(megParent,megList);
					returnMap.put(megGGParent,parentMap);
				} else if (!returnMap.get(megGGParent).keySet().contains(megParent)){
					final ArrayList<File> megList = new ArrayList<File>();
					megList.add(zipFile);
					zipI.remove();
					returnMap.get(megGGParent).put(megParent,megList);
				} else {
					returnMap.get(megGGParent).get(megParent).add(zipFile);
					zipI.remove();
				}
			}
		}
		
		return returnMap;
		
	}

	private String newExpLbl() {
		final String lbl = subjectLbl + DEFAULT_EXT;
       	StringBuilder tryLbl = new StringBuilder(lbl);
        ArrayList<XnatMegsessiondata> al=XnatMegsessiondata.getXnatMegsessiondatasByField("xnat:megSessionData/label",lbl, user, false);
        int i=2;
        while (al.size()>0) {
        	tryLbl = new StringBuilder(lbl);
        	tryLbl.append(i);
        	al=XnatMegsessiondata.getXnatMegsessiondatasByField("xnat:megSessionData/label",tryLbl.toString(), user, false);
        	i++;
        }
        return tryLbl.toString();
	}

	private boolean isMegFile(File zipFile) {
		if (!zipFile.isFile()) {
			return false;
		}
		for (final String compare : Arrays.asList(MEG_FILES)) {
			
			if (zipFile.getName().matches(compare)) {
				return true;
			}
		}
		for (final File f : zipFile.getParentFile().listFiles()) {
			if (f.equals(zipFile)) {
				continue;
			}
			for (final String compare : Arrays.asList(MEG_FILES)) {
				if (zipFile.getName().matches(compare)) {
					return true;
				}
			}
		}
		return false;
	}

	private ArrayList<File> getFileListFromDir(File source) {
		ArrayList<File> fileList = new ArrayList<File>();
		if (!source.isDirectory()) {
			fileList.add(source);
			return fileList;
		}
		for (final File f : source.listFiles()) {
			if (f.isFile()) {
				fileList.add(f);
			} else if (f.isDirectory()) {
				fileList.addAll(getFileListFromDir(f));
			}
		}
		return fileList;
	}

	public void setSubjectLbl(String subjectLbl) {
		this.subjectLbl = subjectLbl;
	}

	public String getSubjectLbl() {
		return subjectLbl;
	}

	public void setExpLbl(String expLbl) {
		this.expLbl = expLbl;
	}

	public String getExpLbl() {
		return expLbl;
	}

	public void setMegDirectory(File megDirectory) {
		this.megDirectory = megDirectory;
	}

	public File getMegDirectory() {
		return megDirectory;
	}
		
	public void setScanList(ArrayList<MEGScan> scanList) {
		this.scanList=scanList;
	}
	
	public ArrayList<MEGScan> getScanList() {
		return scanList;
	}
	
	public class MEGScan implements Comparable<HCPMegRepresentation.MEGScan> {
		
		private String scanID; 
		private String scanLbl; 
		private ArrayList<MEGSession> sessionList; 
		
		public void setScanID(String scanID) {
			this.scanID=scanID;
		}
		
		public String getScanID() {
			return scanID;
		}
		
		public void setScanLbl(String scanLbl) {
			this.scanLbl=scanLbl;
		}
		
		public String getScanLbl() {
			return scanLbl;
		}
		
		public void setSessionList(ArrayList<MEGSession> sessionList) {
			this.sessionList=sessionList;
		}
		
		public ArrayList<MEGSession> getSessionList() {
			return sessionList;
		}
		
		@Override
		public int compareTo(MEGScan cscan) {
			if (this.getSessionList().get(0)!=null && cscan.getSessionList().get(0)!=null) {
				return this.getSessionList().get(0).getSessionDateAsDate().compareTo(cscan.getSessionList().get(0).getSessionDateAsDate());
			} else {
				// Should never happen
				return 0;
			}
		}

	}
	
	public class MEGSession implements Comparable<HCPMegRepresentation.MEGSession> {
		
		private String sessionID; 
		private String sessionDateStr; 
		private ArrayList<MEGRun> runList; 
		
		public void setSessionID(String sessionID) {
			this.sessionID=sessionID;
		}
		
		public String getSessionID() {
			return sessionID;
		}
		
		public void setSessionDateStr(String sessionDateStr) {
			this.sessionDateStr=sessionDateStr;
		}
		
		public String getSessionDateStr() {
			return sessionDateStr;
		}
		
		public void setRunList(ArrayList<MEGRun> runList) {
			this.runList=runList;
		}
		
		public ArrayList<MEGRun> getRunList() {
			return runList;
		}
		
		public Date getSessionDateAsDate() {
			try {
				return SDA_DF.parse(getSessionDateStr());
			} catch (ParseException e1) {
				return null;
			}
		}
		
		@Override
		public int compareTo(MEGSession csess) {
			return this.getSessionDateAsDate().compareTo(csess.getSessionDateAsDate());
		}

	}

	public class MEGRun {
		
		private String runID; 
		private String runLbl; 
		private ArrayList<MEGFile> fileList; 
		
		public void setRunID(String runID) {
			this.runID=runID;
		}
		
		public String getRunID() {
			return runID;
		}
		
		public void setRunLbl(String runLbl) {
			this.runLbl=runLbl;
		}
		
		public String getRunLbl() {
			return runLbl;
		}
		
		public void setFileList(ArrayList<MEGFile> fileList) {
			this.fileList=fileList;
		}
		
		public ArrayList<MEGFile> getFileList() {
			return fileList;
		}

	}

	public class MEGFile {
		
		private String fileType; 
		private String fileIn; 
		private String fileOut; 
		
		public void setFileType(String fileType) {
			this.fileType = fileType;
		}

		public String getFileType() {
			return fileType;
		}

		public void setFileIn(String fileIn) {
			this.fileIn = fileIn;
		}

		public String getFileIn() {
			return fileIn;
		}

		public void setFileOut(String fileOut) {
			this.fileOut = fileOut;
		}

		public String getFileOut() {
			return fileOut;
		}

	}

	public static String dequote(String str) {
		final int len = str.length();
		if (len > 1 && str.charAt(0) == '\"' && str.charAt(len - 1) == '\"') {
			return str.substring(1, len - 1).replaceAll("\\\"", "\"");
		}
		return str;
	}
	
	
	public List<String> buildMegArchiveSession(XnatProjectdata proj, UserI user, File destination) throws ClientException {
		
		this.proj = proj;
		
		if (!destination.exists()) {
			throw new ClientException("ERROR:  Destination directory does not exist.",new Exception());
		}
		final File sessionDir = new File(destination,expLbl + File.separator + "SCANS");
		sessionDir.mkdirs();
		File resourceDir = null;
		// For now, don't create INFO resource when building under the new format
		if (dirStructure==null || dirStructure.keySet().size()<=0) {
			resourceDir = new File(destination,expLbl + File.separator + "RESOURCES" + File.separator + "INFO");
			resourceDir.mkdirs();
		}
		
		final XnatMegsessiondataBean session = new XnatMegsessiondataBean();
		
		final XnatSubjectdata subj=XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(),subjectLbl, user, false);
		if (subj == null) {
			throw new ClientException("ERROR:  Could not build session, subject " + subjectLbl + " does not exist.",new Exception());
		}
		final XnatExperimentdata existing=XnatExperimentdata.GetExptByProjectIdentifier(proj.getId(), expLbl, user, false);
		if (existing != null) {
			throw new ClientException("ERROR:  Experiment specified in the MEG archive (" + expLbl + ") already exists.",new Exception());
		}
		
		returnList.add("Creating session (SUBJECT=" + subj.getId() + ",SESSION=" + expLbl);
		
		session.setSubjectId(subj.getId());
		session.setProject(proj.getId());
		session.setLabel(expLbl);
		session.setModality("MEG");
		session.setAcquisitionSite("SLU");
		session.setScanner("SLU MEG");
		
		if (dirStructure!=null && dirStructure.keySet().size()>0) {
			return buildMegArchiveSessionFromDirStructure(session,user,destination,sessionDir,resourceDir);
		} else if (scanList!=null && scanList.size()>0) {
			return buildMegArchiveSessionFromScanList(session,user,destination,sessionDir,resourceDir);
		} else {
			throw new ClientException("Uploaded file does not contain a valid MEG session or is not structured according to expectations.");
		}
	}
	
	private Date dirToDateTime(File dir) {
		if (dir.isDirectory()) {
			try {
				final Date returnDate = DDT_DF.parse(dir.getName().replaceAll(DDT_SUBST,"%"));
				if (returnDate!=null) {
					return returnDate;
				}
			} catch (ParseException e) { }
		} 
		return null;
	}
		
	private Date dirStructureToDate(SortedMap<File, SortedMap<File, ArrayList<File>>> dirStructure) {
		for (final File f1 : dirStructure.keySet()) {
			for (final File f2 : dirStructure.get(f1).keySet()) {
				final String dirName = f2.getParentFile().getName();
				try {
					final Date returnDate = STD_DF.parse(dirName);
					if (returnDate!=null) {
						return returnDate;
					}
				} catch (ParseException e) {
					// Do nothing, try next file
				}
			}
		}
		return null;
	}
		
	
	public List<String> buildMegArchiveSessionFromDirStructure(XnatMegsessiondataBean session, UserI user, File destination, File sessionDir, File resourceDir) throws ClientException {
		
		// Set scan date from directory name
		session.setDate(dirStructureToDate(dirStructure));
		
		if (resourceDir!=null) {
			final CatCatalogBean ccat = new CatCatalogBean();
			final XnatResourcecatalogBean scat = new XnatResourcecatalogBean();
			try {
				final File resourceFile = new File(resourceDir,"scaninfo_catalog.xml");
				final FileWriter fw = new FileWriter(resourceFile);
				ccat.toXML(fw);
				fw.close();
				// Set URI to archive path
				scat.setUri(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + expLbl +
								File.separator + Utils.getRelativeURI(sessionDir.getParentFile(),resourceFile));
			} catch (IOException e) {
				throw new ClientException("Couldn't write catalog XML file",e);
			} catch (Exception e) {
				throw new ClientException("Couldn't write catalog XML file",e);
			}
			scat.setLabel("INFO");
			scat.setFormat("MISC");
			scat.setContent("INFO");
			session.addResources_resource(scat);
		}
		
		int scanNo = 0;
		final HashMap<String,Integer> sdMap = new HashMap<String,Integer>();
		
		for (final File ggpf : dirStructure.keySet()) {
			for (final File pf : dirStructure.get(ggpf).keySet()) {
				
				scanNo++;
				final List<File> fileList = dirStructure.get(ggpf).get(pf);
					
				final File scanDir = new File(sessionDir,scanNo + File.separator + "4D");
				scanDir.mkdirs();
				//final File runInfoDir = new File(sessionDir,scanNo + File.separator + "INFO");
				//runInfoDir.mkdirs();
				
				final XnatMegscandataBean scan = new XnatMegscandataBean();
				scan.setId(Integer.toString(scanNo));
				
				final StringBuilder seriesDescSB = new StringBuilder(getBaseDescriptionFromFileName(ggpf.getName()));
				
				final String scanType = (seriesDescSB.indexOf("_")>=0) ? seriesDescSB.toString().substring(0,seriesDescSB.indexOf("_")) : seriesDescSB.toString();
				
				if ( !Arrays.asList(TASK_STRINGS).contains(scanType) && getSeriesDescCount(seriesDescSB.toString())>1 ) {
					int runNo = getRunNo(seriesDescSB.toString(),sdMap);
					seriesDescSB.append(runNo).toString();
				}
				scan.setType(scanType);
				scan.setSeriesDescription(seriesDescSB.toString());
				final Date scanDT = dirToDateTime(pf.getParentFile());
				if (scanDT!=null) {
					scan.setStarttime(TIME_FMT.format(scanDT));
				}
				
				// Move files from source to dest.
				final File catFile = new File(scanDir,"scan_" + scanNo + "_catalog.xml");
				final CatCatalogBean scanCat = new CatCatalogBean();
				
				boolean isUsable = true;
				
				// Check that number of files matches expected (NOTE:  This list has been populated by file name, so we don't heed to re-check filenames)
				if (Arrays.asList(NOISE_STRINGS).contains(scanType)) {
					if (fileList.size() != NOISE_FILES.length) {
						isUsable = false;
					}
				} else if (Arrays.asList(TASK_STRINGS).contains(scanType) || Arrays.asList(OTHER_STRINGS).contains(scanType)) {
					if (fileList.size() != MEG_FILES.length) {
						isUsable = false;
					}
				}
				String scanNote = isUsable ? "" : "Quality set to unusable by importer due to unexpected number of files for scan (" + fileList.size() + ")";
				
				for (final File mfile : fileList) {
					returnList.add("Uploading file to scan (SCAN=" + scanNo + ", SD=" + seriesDescSB.toString() + ", FILE=" + mfile.getName());
					// Check filesize against expected
					//final String mfn = mfile.getName();
					/*
					 // Comment out this section for now.  Per 2013/08/07 e-mail, Linda is okay with not checking file size.  
					final Map<String,Long[]> scanmap = fileSizeMap.get(scanType);
					if (scanmap!=null) {
						final Long[] sizes = scanmap.get(mfn);
						long mfsize = mfile.length();
						if (sizes!=null && (mfsize<sizes[0] || mfsize>sizes[1])) {
							scanNote = "Quality set to unusable by importer due to unexpected file size (FILE=" + mfn + ", SIZE=" + mfsize + ")";
							isUsable = false;
						}
					}
					*/
					addFileToCatalog(scanCat,mfile,scanDir,true);
				}
				
				if (isUsable) {
					scan.setQuality("usable");
				} else {
					scan.setQuality("unusable");
					scan.setNote(scanNote);
				}
				
				final XnatResourcecatalogBean rcat = new XnatResourcecatalogBean();
				writeCatalogXML(scanCat,rcat,catFile,sessionDir.getParentFile());
				rcat.setLabel("4D");
				rcat.setFormat("4D");
				rcat.setContent("RAW");
				scan.addFile(rcat);
				
				if ((!(eegFiles==null || eegFiles.size()==0)) && seriesDescSB.toString().contains(EEG_INCLUDE)) {
					
					final File eegDir = new File(sessionDir,scanNo + File.separator + "EEG");
					eegDir.mkdirs();
					
					final File eegCatFile = new File(eegDir,"eeg_" + scanNo + "_catalog.xml");
					CatCatalogBean eegCat = new CatCatalogBean();
				
					// Per abbas add additional (EEG) files to scan
					for (final File efile : eegFiles) {
						returnList.add("Uploading file to scan (SCAN=" + scanNo + ", SD=" + seriesDescSB.toString() + ", FILE=" + efile.getName());
						addFileToCatalog(eegCat,efile,eegDir,true);
					}
				
					final XnatResourcecatalogBean ecat = new XnatResourcecatalogBean();
					writeCatalogXML(eegCat,ecat,eegCatFile,sessionDir.getParentFile());
					ecat.setLabel("EEG");
					ecat.setFormat("4D");
					ecat.setContent("RAW");
					scan.addFile(ecat);
					
				}
				
				/* Don't create info resource for now
			
				final File runInfoFile = new File(runInfoDir,"info_" + scanNo + "_catalog.xml");
				CatCatalogBean infoCat = new CatCatalogBean();
				
				final XnatResourcecatalogBean icat = new XnatResourcecatalogBean();
				writeCatalogXML(infoCat,icat,runInfoFile,sessionDir.getParentFile());
				icat.setLabel("INFO");
				icat.setFormat("MISC");
				icat.setContent("INFO");
				scan.addFile(icat);
				
				 */
				
				session.addScans_scan(scan);
				
				populateScanTypeMaps(scan);
				
			}
		}
		
		// Add E-Prime files to scans
		if (checkEprimeMatches()) {
			
			for (final String stype : scanTypeMap.keySet()) {
				
				TreeSet<XnatMegscandataBean> sset = scanTypeMap.get(stype);
				final TreeSet<File> fset = eprimeTypeMap.get(stype);
				if (fset == null || fset.isEmpty()) {
					// NO E-PRIME FILES UPLOADED FOR THIS TYPE
					for (final XnatMegscandataBean scan : sset) {
						addNoteToScan(scan);
					}
					continue;
				}
				if (sset.size()!=fset.size()) {
					sset = usableScanTypeMap.get(stype);
				}
				final Iterator<File> fiter = fset.iterator();
				//boolean typeHasEprime = false;
				for (final XnatMegscandataBean scan : sset) {
					
					final File edatf = fiter.next();
					final File textf = eprimeMap.get(edatf);
					if (textf == null) {
						returnList.add("WARNING:  Could not find matching txt file for edat file " + edatf.getName() +
										".  E-prime must be uploaded manually for scan.");
						addNoteToScan(scan);
						continue;
					}
					//typeHasEprime = true;
					// pull run number from edat file
					final String run_number = edatf.getName().substring(0,edatf.getName().indexOf(".edat2")).replaceAll("^.*[^0-9]","");
					if (run_number.length()>0 && !(scan.getSeriesDescription()!=null && scan.getSeriesDescription().matches("^.*[0-9]$"))) {
						scan.setSeriesDescription(scan.getSeriesDescription()+run_number);
					}
					final CatCatalogBean cat = new CatCatalogBean();
					final String uriDir = "EPRIME";
					final File linkedDataDir = new File(sessionDir,Integer.parseInt(scan.getId()) + File.separator + "LINKED_DATA");
					final File eprimeDir = new File(sessionDir,Integer.parseInt(scan.getId()) + File.separator + "LINKED_DATA/" + uriDir);
					linkedDataDir.mkdirs();
					final File linkedDataFile = new File(linkedDataDir,"info_" + Integer.parseInt(scan.getId()) + "_catalog.xml");
					
					returnList.add("Uploading e-prime file to scan (SCAN=" + scanNo + ", SD=" + scan.getSeriesDescription() + ", FILE=" + edatf.getName());
					addFileToCatalog(cat,edatf,eprimeDir,uriDir,true);
					returnList.add("Uploading e-prime file to scan (SCAN=" + scanNo + ", SD=" + scan.getSeriesDescription() + ", FILE=" + textf.getName());
					addFileToCatalog(cat,textf,eprimeDir,uriDir,true);
				
					final XnatResourcecatalogBean icat = new XnatResourcecatalogBean();
					writeCatalogXML(cat,icat,linkedDataFile,sessionDir.getParentFile());
					icat.setLabel("LINKED_DATA");
					icat.setFormat("MISC");
					icat.setContent("RAW");
					scan.addFile(icat);
				
				}
			}
		
		}
		
       	// Output session XML
       	final File destFile = new File(destination,expLbl + ".xml");
        try {
			final FileWriter fw = new FileWriter(destFile);
			session.toXML(fw);
			fw.close();
			
		} catch (Exception e) {
            throw new ClientException("Couldn't write output XML file",e);
		}
			
		try {
			// Save Session XML
			final SAXReader reader = new SAXReader(user);
			final XFTItem item = reader.parse(destFile);
			final XnatMegsessiondata megdat = (XnatMegsessiondata)BaseElement.GetGeneratedItem(item);
			megdat.setId(XnatExperimentdata.CreateNewID());
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, megdat.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
			final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(megdat,user,false,false,ci)) {
				PersistentWorkflowUtils.complete(wrk, ci);
			} else {
				PersistentWorkflowUtils.fail(wrk,ci);
			}
			
			// Move Files
			try {
				FileUtils.moveDirectoryToDirectory(sessionDir.getParentFile(),
					new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc()),
					true);
			} catch (Exception e) {
				final PersistentWorkflowI dwrk = PersistentWorkflowUtils.buildOpenWorkflow(user, megdat.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.DELETE_VIA_WEB_SERVICE, null, null));
				final EventMetaI dci = dwrk.buildEvent();
				SaveItemHelper.authorizedDelete(megdat.getItem(),user,dci);
				throw new ClientException("Couldn't move files to archive",new Exception());
			}
			
			returnList.add(0,"/archive/experiments/" + megdat.getId());
			refreshFileCountsInDB(megdat);
			return returnList;
			
		} catch (Exception e) {
            throw new ClientException("Couldn't save session",e);
		}
	}
	
	private void addNoteToScan(XnatMegscandataBean scan) {
		if ((scan.getQuality()!=null && scan.getQuality().equalsIgnoreCase("unusable")) ||
				(scan.getSeriesDescription()!=null && scan.getSeriesDescription().matches("^.*[0-9]$"))) {
			return;
		}
		final String currNote = scan.getNote();
		scan.setNote((currNote==null || currNote.length()<1)  ? NO_EPRIME_STR : currNote + ", " + NO_EPRIME_STR);
	}

	private boolean checkEprimeMatches() {
			
		boolean hasEprime = false;
		for (final String stype : scanTypeMap.keySet()) {
			TreeSet<XnatMegscandataBean> sset = scanTypeMap.get(stype);
			final TreeSet<File> fset = eprimeTypeMap.get(stype);
			if (fset == null) {
				// NO E-PRIME FILES UPLOADED FOR THIS TYPE
				continue;
			}
			if (sset.size() != fset.size()) {
				returnList.add("WARNING:  Number of eprime files for " + stype + " (" + fset.size() + ") does not match the number of scans (" +
								sset.size() + ").  Will try matching to usable scans.");
				sset = usableScanTypeMap.get(stype);
				if (sset.size() != fset.size()) {
					returnList.add("ERROR:  Number of eprime files for " + stype + " (" + fset.size() + ") does not match the number usable or unusable scans (" +
									sset.size() + ").  E-PRIME files will not be uploaded.");
					return false;
				}
			}
			hasEprime = true;
		}
		return hasEprime;
			
	}

	@SuppressWarnings("unused")
	private int getSeriesDescCount(String ins) {
		if (sdCountMap==null) {
			sdCountMap = new HashMap<String,Integer>();
			for (final File ggpf : dirStructure.keySet()) {
				for (final File pf : dirStructure.get(ggpf).keySet()) {
					final String seriesDesc = getBaseDescriptionFromFileName(ggpf.getName());
					if (sdCountMap.get(seriesDesc)==null) {
						sdCountMap.put(seriesDesc,1);
					} else {
						sdCountMap.put(seriesDesc,sdCountMap.get(seriesDesc)+1);
					}
				}
			}
		}
		if (sdCountMap.get(ins)!=null) {
			return sdCountMap.get(ins);
		}
		return 1;
	}

	private void writeCatalogXML(CatCatalogBean cat, XnatResourcecatalogBean rcat, File runInfoFile, File dir) throws ClientException {
		try {
			final FileWriter fw = new FileWriter(runInfoFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			rcat.setUri(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + expLbl +
							File.separator + Utils.getRelativeURI(dir,runInfoFile));
		} catch (IOException e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		}
	}

	private void addFileToCatalog(CatCatalogBean cat, File f, File dir, boolean moveFile) throws ClientException {
		addFileToCatalog(cat,f,dir,null,moveFile);
	}

	private void addFileToCatalog(CatCatalogBean cat, File f, File dir,String uriDir, boolean moveFile) throws ClientException {
		final File outFile = new File(dir,f.getName());
		try {
			if (moveFile) {
				FileUtils.moveFile(f,outFile);
			} else {
				FileUtils.copyFile(f,outFile);
			}
			final CatEntryBean catEntry = new CatEntryBean();
			catEntry.setName(outFile.getName());
			StringBuilder uriSB = new StringBuilder();
			if (uriDir!=null) {
				uriSB.append(uriDir);
				if (!uriDir.endsWith("/")) {
					uriSB.append("/");
				}
			}
			uriSB.append(outFile.getName());
			catEntry.setUri(uriSB.toString());
			cat.addEntries_entry(catEntry);
		} catch (IOException e) {
			throw new ClientException(e.getMessage(),new Exception());
		}
	}

	private int getRunNo(String seriesDesc, HashMap<String, Integer> sdMap) {
		Integer i = sdMap.get(seriesDesc);
		i = (i==null) ? 1 : i+1;
		sdMap.put(seriesDesc,i);
		return i;
	}

	private String getBaseDescriptionFromFileName(String name) {
		for (final String conv : convertMap.keySet()) {
			if (name.contains(conv)) {
				return convertMap.get(conv);
			}
		}
		return name;
	}

	public List<String> buildMegArchiveSessionFromScanList(XnatMegsessiondataBean session, UserI user, File destination, File sessionDir, File resourceDir) throws ClientException {
		
		session.setDate(this.getScanList().get(0).getSessionList().get(0).getSessionDateAsDate());
		if (session.getDate()==null) {
			session.setDate(this.getScanList().get(0).getSessionList().get(0).getSessionDateStr());
		}
					
		// Save TOC file in scaninfo directory
		if (resourceDir!=null) {
			final CatCatalogBean ccat = new CatCatalogBean();
			addFileToCatalog(ccat,tocFile,resourceDir,false);
					
			final XnatResourcecatalogBean scat = new XnatResourcecatalogBean();
			try {
				final File resourceFile = new File(resourceDir,"scaninfo_catalog.xml");
				final FileWriter fw = new FileWriter(resourceFile);
				ccat.toXML(fw);
				fw.close();
				// Set URI to archive path
				scat.setUri(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + expLbl +
								File.separator + Utils.getRelativeURI(sessionDir.getParentFile(),resourceFile));
			} catch (IOException e) {
				throw new ClientException("Couldn't write catalog XML file",e);
			} catch (Exception e) {
				throw new ClientException("Couldn't write catalog XML file",e);
			}
			scat.setLabel("INFO");
			scat.setFormat("MISC");
			scat.setContent("INFO");
			session.addResources_resource(scat);
		}
		
		int scanNo = 0;
		
		// Iterate over supplied scans, sessions & runs, outputting each run as an XNAT scan catalog
		for (final MEGScan mscan : this.getScanList()) {
			for (final MEGSession msess : mscan.getSessionList()) {
				for (final MEGRun mrun : msess.getRunList()) {
					
					scanNo++;
					final File scanDir = new File(sessionDir,scanNo + File.separator + "4D");
					scanDir.mkdirs();
					//final File runInfoDir = new File(sessionDir,scanNo + File.separator + "INFO");
					//runInfoDir.mkdirs();
					
					final XnatMegscandataBean scan = new XnatMegscandataBean();
					scan.setId(Integer.toString(scanNo));
					final StringBuilder scanTypeSB = new StringBuilder(mscan.getScanLbl());
					if (mscan.getSessionList().size()>1) {
						scanTypeSB.append("_").append(msess.getSessionID());
					}
					final StringBuilder seriesDescSB = new StringBuilder(scanTypeSB);
					if (msess.getRunList().size()>1) {
						seriesDescSB.append("_").append("Run" + mrun.getRunLbl()).toString();
					}
					scan.setType(scanTypeSB.toString());
					scan.setSeriesDescription(seriesDescSB.toString());
					scan.setQuality("usable");
					
					// Copy files from source to dest.
					final File sourceDir = new File(megDirectory,new StringBuilder(patientID).append(File.separator).append(mscan.getScanID())
									.append(File.separator).append(msess.getSessionID()).append(File.separator).append(mrun.getRunID()).toString());
					final File catFile = new File(scanDir,"scan_" + scanNo + "_catalog.xml");
					//final File runInfoFile = new File(runInfoDir,"info_" + scanNo + "_catalog.xml");
					
					CatCatalogBean cat = new CatCatalogBean();
					
					for (final MEGFile mfile : mrun.getFileList()) {
						File sourceFile = new File(sourceDir,mfile.getFileIn());
						if (!sourceFile.exists()) {
							throw new ClientException("ERROR:  Source file structure is invalid or not currently supported by uploader.",new Exception());
						}
						returnList.add("Uploading file to scan (SCAN=" + scanNo + ", SD=" + seriesDescSB.toString() + ", FILE=" + sourceFile.getName());
						final File outFile = new File(scanDir,mfile.getFileOut());
						try {
							FileUtils.copyFile(sourceFile,outFile);
							final CatEntryBean catEntry = new CatEntryBean();
							catEntry.setName(outFile.getName());
							catEntry.setUri(outFile.getName());
							// Let's not set format on files for now
							//catEntry.setFormat("BINARY");
							//catEntry.setContent("RAW");
							cat.addEntries_entry(catEntry);
							
						} catch (IOException e) {
							throw new ClientException(e.getMessage(),new Exception());
						}
					}
					
					final XnatResourcecatalogBean rcat = new XnatResourcecatalogBean();
					writeCatalogXML(cat,rcat,catFile,sessionDir.getParentFile());
					rcat.setLabel("4D");
					rcat.setFormat("BINARY");
					rcat.setContent("RAW");
					scan.addFile(rcat);
				
					/* Don't create info resource for now
					cat = new CatCatalogBean();
					
					final XnatResourcecatalogBean icat = new XnatResourcecatalogBean();
					writeCatalogXML(cat,icat,runInfoFile,sessionDir.getParentFile());
					icat.setLabel("INFO");
					icat.setFormat("MISC");
					icat.setContent("INFO");
					scan.addFile(icat);
					*/
					
					session.addScans_scan(scan);
					
				}
			}
		}
		
       	// Output session XML
       	final File destFile = new File(destination,expLbl + ".xml");
        try {
			final FileWriter fw = new FileWriter(destFile);
			session.toXML(fw);
			fw.close();
			
		} catch (Exception e) {
            throw new ClientException("Couldn't write output XML file",e);
		}
			
		try {
			// Save Session XML
			final SAXReader reader = new SAXReader(user);
			final XFTItem item = reader.parse(destFile);
			final XnatMegsessiondata megdat = (XnatMegsessiondata)BaseElement.GetGeneratedItem(item);
			megdat.setId(XnatExperimentdata.CreateNewID());
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, megdat.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
			final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(megdat,user,false,false,ci)) {
				PersistentWorkflowUtils.complete(wrk, ci);
			} else {
				PersistentWorkflowUtils.fail(wrk,ci);
			}
			
			// Move Files
			try {
				FileUtils.moveDirectoryToDirectory(sessionDir.getParentFile(),
					new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc()),
					true);
			} catch (Exception e) {
				final PersistentWorkflowI dwrk = PersistentWorkflowUtils.buildOpenWorkflow(user, megdat.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.DELETE_VIA_WEB_SERVICE, null, null));
				final EventMetaI dci = dwrk.buildEvent();
				SaveItemHelper.authorizedDelete(megdat.getItem(),user,dci);
				throw new ClientException("Couldn't move files to archive",new Exception());
			}
			
			returnList.add(0,"/archive/experiments/" + megdat.getId());
			refreshFileCountsInDB(megdat);
			return returnList;
			
		} catch (Exception e) {
            throw new ClientException("Couldn't save session",e);
		}
	}
	
	/////////////////////////
	// MOTION FILE METHODS //
	/////////////////////////

	private void refreshFileCountsInDB(XnatImagesessiondata exp) {
		
		final String projectPath = ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc();
		// Refresh session-level resources
		for (final XnatAbstractresourceI rs : exp.getResources_resource()) {
			refreshCounts(exp.getItem(),rs,projectPath);
		}
		// Refresh scan-level resources
		List<XnatImagescandata> scans = exp.getScansByXSIType("xnat:megScanData");
		for (final XnatImagescandata scan : scans) {
			for (final XnatAbstractresourceI rs : scan.getFile()) {
				refreshCounts(scan.getItem(),rs,projectPath);
			}
		}
	}
	
	public void refreshCounts(XFTItem it,XnatAbstractresourceI resource,String projectPath) {
		try {
			// Clear current file counts and sizes so they will be recomputed instead of pulled from existing values
			if (resource instanceof XnatResourcecatalog) {
				((BaseXnatResourcecatalog)resource).clearCountAndSize();
				((XnatResourcecatalog)resource).clearFiles();
			}
			final CatalogService _catService = XDAT.getContextService().getBean(CatalogService.class);
			_catService.refreshResourceCatalog(user, ((XnatResourcecatalog)resource).getUri(), CatalogService.Operation.ALL);
			
		} catch (Exception e) {
			returnList.add("WARNING:  Could not update file counts for item");
		}
	}

}
