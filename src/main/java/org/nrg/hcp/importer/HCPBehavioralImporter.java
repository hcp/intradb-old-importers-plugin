package org.nrg.hcp.importer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.hcp.utils.NontoolboxAsrScoringUtil;
import org.nrg.hcp.utils.CsvParser;
import org.nrg.xdat.om.*;
import org.nrg.xdat.om.base.auto.AutoXnatProjectdata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.InvalidValueException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.Wrappers.GenericWrapper.GenericWrapperField;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.restlet.actions.importer.ImporterHandler;
import org.nrg.xnat.restlet.actions.importer.ImporterHandlerA;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.python.google.common.collect.Lists;

/*
 * NOTES:  Per e-mail chain with Deanna on 01/13/2012, we are expecting only ONE entry per subject in the CSV file and only ONE entry
 * saved in the database per subject.  This program is based on that assumption.  Per Deanna: 
 * 
 *    "So, datasetid, siteid and famid are going to be assigned by gur and are basically irrelevant to us.  The HCP numbr 
 *     will be input into subid.  The way it SHOULD work is that there will ever only be oe record per person.  If the person
 *     has to quit and start again, we should be able to go right back into their same record.  That is my understanding of 
 *     how that will work.   By backup, we just meant that they might have to finish it later, but it should still be only
 *     one record per subject."
 *     
 * If things change and there could be multiple CSV and/or database records, this program will require modifications to handle 
 * this.    
 *
 */

/**
 * Imports HCP GUR data and populates database.
 * @author Mike Hodge <hodgem@mir.wustl.edu>
 *
 */
@ImporterHandler(handler = "BEH", allowCallsWithoutFiles = false, callPartialUriWrap = false)
public class HCPBehavioralImporter extends ImporterHandlerA implements Callable<List<String>> {

	static Logger logger = Logger.getLogger(HCPBehavioralImporter.class);

	private final FileWriterWrapperI fw;
	private final UserI user;
	final Map<String,Object> params;
   	private XnatProjectdata proj;
   	private boolean updateExisting = false;
   	private boolean verboseOutput = false;
   	private ArrayList<String> csvSubjLbls = new ArrayList<String>();
   	private ArrayList<String> dbAsrSubjLbls = new ArrayList<String>();
   	private ArrayList<String> dbNeoSubjLbls = new ArrayList<String>();
   	private ArrayList<String> dbNtSubjLbls = new ArrayList<String>();
    private ArrayList<String> dbTbSubjLbls = new ArrayList<String>();
    private ArrayList<String> dbSsagaSubjLbls = new ArrayList<String>();
   	private ArrayList<String> newAsrSubjLbls = new ArrayList<String>();
   	private ArrayList<String> newNeoSubjLbls = new ArrayList<String>();
   	private ArrayList<String> newNtSubjLbls = new ArrayList<String>();
    private ArrayList<String> newTbSubjLbls = new ArrayList<String>();
   	private ArrayList<String> returnList = new ArrayList<String>();
   	
   	// value prepended to ASR fields in CSV file  
   	private static final String asr_prepend = "ASRVIII.";
   	private static final String neo_prepend = "NEO.";
   	
	// Retest project records use a prepended subject label
   	private static final String RETEST_PROJECT = "Phase2_Retest";
   	private static final String RETEST_ID_PREPEND = "RT";
   	
   	// NOTE:  Toolbox added columns for ER40 kids tests, but we want to keep them in the same fields.  We'll use this map to help handle that
    static final Map<String,String> er40Map;
    static { 
        final Map<String,String> map = new HashMap<String,String>();
        map.put("ER40D.test","K_ER40D.test");
        map.put("ER40D.valid_code","K_ER40D.valid_code");
        map.put("ER40D.ER40_CR","K_ER40D.ER40_CR");
        map.put("ER40D.ER40_CRT","K_ER40D.ER40_CRT");
        map.put("ER40D.ER40ANG","K_ER40D.ER40ANG");
        map.put("ER40D.ER40FEAR","K_ER40D.ER40FEAR");
        map.put("ER40D.ER40HAP","K_ER40D.ER40HAP");
        map.put("ER40D.ER40NOE","K_ER40D.ER40NOE");
        map.put("ER40D.ER40SAD","K_ER40D.ER40SAD");
        er40Map = Collections.unmodifiableMap(map);
    }

    static final Map<String,String> toolboxTestMap;
    static { // Maps the Toolbox form names to the HCP Toolbox schema elements
        final Map<String,String> map = new HashMap<String,String>();
        map.put("nihtbdimensionalchangecardsortage3", "cog_dim_card");
        map.put("nihtbflankerinhibitorycontrolandattnage3", "cog_flanker");
        map.put("nihtblistsortingworkingmemoryage7", "cog_list_sort");
        map.put("nihtboralreadingrecognitionengage7", "cog_oral_read");
        map.put("nihtbpatterncomparisonprocessingspeedage7", "cog_pattern");
        map.put("nihtbpatterncomparisonprocessspeed7v11", "cog_pattern");
        map.put("nihtbpicturesequencememoryage3", "cog_pic_seq");
        map.put("nihtbpicturevocabularyage3", "cog_pic_vocab");
        map.put("nihtbfearsomaticarousalsfage18", "emo_fear-som");
        map.put("nihtbfriendshipsfage8", "emo_friend");
        map.put("nihtbgenerallifesatisfactioncatage18", "emo_life_sat");
        map.put("nihtbgenerallifesatisfactioncatages1317", "emo_life_sat"); // Lifespan age range
        map.put("nihtbgenerallifesatisfactionsfages812", "emo_life_sat"); // Lifespan age range
        map.put("nihtbinstrumentalsupportsfage18", "emo_inst_sup");
        map.put("nihtblonelinesssfage8", "emo_lonely");
        map.put("nihtbmeaningandpurposecatage18", "emo_meaning");
        map.put("nihtbperceivedhostilitysfage8", "emo_hostil");
        map.put("nihtbperceivedrejectionsfage8", "emo_reject");
        map.put("nihtbperceivedstresscatage18", "emo_stress");
        map.put("nihtbperceivedstresscatages1317", "emo_stress"); // Lifespan age range
        map.put("nihtbpositiveaffectcatage18", "emo_pos_affect");
        map.put("nihtbpositiveaffectcatages1317", "emo_pos_affect"); // Lifespan age range
        map.put("nihtbpositiveaffectsfages812", "emo_pos_affect"); // Lifespan age range
        map.put("nihtbsadnesscatage18", "emo_sadness");
        map.put("nihtbsadnesssfages817", "emo_sadness"); // Lifespan age range and naming
        map.put("nihtbselfefficacycatage18", "emo_self-eff");
        map.put("nihtbselfefficacycatages1317", "emo_self-eff"); // Lifespan age range
        map.put("nihtbselfefficacycatages812", "emo_self-eff"); // Lifespan age range
        map.put("nihtbangeraffectcatage18", "emo_anger-aff");
        map.put("nihtbangersfages817", "emo_anger_sf"); // New in Lifespan
        map.put("nihtbangerhostilitysfage18", "emo_anger-hostil");
        map.put("nihtbangerphysicalaggressionsfage18", "emo_anger-phys");
        map.put("nihtbemotionalsupportsfage8", "emo_support");
        map.put("nihtbfearaffectcatage18", "emo_fear-aff");
        map.put("nihtbfearsfages817", "emo_fear_sf"); // New in Lifespan
        map.put("nihtbgripstrengthage3", "mot_grip");
        map.put("nihtb2minutewalkenduranceage3", "mot_walk_2min");
        map.put("nihtb4meterwalkgaitspeedage7", "mot_walk_4m");
        map.put("nihtb9holepegboarddexterityage3", "mot_pegboard");
        map.put("nihtbodoridentificationage3", "sen_odor");
        map.put("nihtbpaininterferencecatage18", "sen_pain");
        map.put("nihtbregionaltasteintensityage12", "sen_taste");
        map.put("nihtbwordsinnoiseage6", "sen_noise");
        // Unused and ignored Toolbox Forms.
        // The rows are present for earlier subjects but associated fields contain no data
        map.put("nihtbflankerinhibitorycontrolattentiontest", "unused");
        map.put("nihtoolboxgripstrengthtest", "unused");
        map.put("nihtoolboxpicturesequencememorytest", "unused");
        map.put("toolboxperceivedhostilitysf8to85", "unused");
        map.put("toolboxperceivedstresscat18to85", "unused");
        map.put("nihtoolbox4meterwalkgaitspeedtest", "unused");
        map.put("nihtoolboxpaininterferencesurvey18to85", "unused");
        map.put("toolboxgenerallifesatisfactioncat18to85", "unused");
        map.put("nihtblistsortingworkingmemorytest", "unused");
        map.put("nihtbwordsinnoisetest", "unused");
        map.put("toolboxmeaningpurposecat18to85", "unused");
        map.put("toolboxlonelinesssf8to85", "unused");
        map.put("toolboxangerhostilitysf18to85", "unused");
        map.put("toolboxemotionalsupportsf8to85", "unused");
        map.put("nihtoolboxregionaltasteintensitytest", "unused");
        map.put("nihtbpatterncomparisonprocessingspeedtest", "unused");
        map.put("toolboxperceivedrejectionsf8to85", "unused");
        map.put("nihtoolbox9holepegboarddexteritytest", "unused");
        map.put("toolboxinstrumentalsupportsf18to85", "unused");
        map.put("nihtboralreadingrecognitiontest", "unused");
        map.put("toolboxfearsomaticarousalsf18to85", "unused");
        map.put("toolboxfearaffectcat18to85", "unused");
        map.put("toolboxpositiveaffectcat18to85", "unused");
        map.put("toolboxsadnesscat18to85", "unused");
        map.put("toolboxfriendshipsf8to85", "unused");
        map.put("toolboxangeraffectcat18to85", "unused");
        map.put("nihtoolbox2minutewalkendurancetest", "unused");
        map.put("toolboxselfefficacycat18to85", "unused");
        map.put("nihtoolboxdimensionalchangecardsorttest", "unused");
        map.put("toolboxangerphysicalaggressionsf18to85", "unused");
        map.put("nihtoolboxodoridentificationtest", "unused");
        map.put("nihtbpicturevocabularytest", "unused");
        // Added 11/1/2013 due to Consent values changing in upload file
        // Update 4/3/2014: included in Lifespan and Phase2 output. No longer "unused"
        map.put("cognitioncrystallizedcomposite", "cog_crystal_comp");
        map.put("cognitionearlychildhoodcomposite", "cog_early_comp");
        map.put("cognitionfluidcomposite", "cog_fluid_comp");
        map.put("cognitiontotalcompositescore", "cog_total_comp");
        // Lifespan Parent Forms
        map.put("nihtbpositiveaffectparentrptcatages37", "par_pos_affect");
        map.put("nihtbpositiveaffectparentrptcatages812", "par_pos_affect");
        map.put("nihtbgenerallifesatisparentrptsfages312", "par_life_sat");
        map.put("nihtbperceivedstressparentrptcatages812", "par_stress");
        map.put("nihtbselfefficacyparentrptcatages812", "par_self-eff");
        map.put("nihtbpospeerinteractparentrptsfages312", "par_peer_interact");
        map.put("nihtbsocialwithdrawalparentrptsfages312", "par_soc_withdrawal");
        map.put("nihtbpeerrejectionparentrptsfages312", "par_peer_reject");
        map.put("nihtbempathicbehaviorsparentrptcatages312", "par_empathy");
        map.put("nihtbfearparentrptcatages812", "par_fear");
        map.put("nihtbfearseparationanxparentrptsfages37", "par_fear_sep_anx");
        map.put("nihtbfearoveranxiousparentrptsfages37", "par_fear_over_anx");
        map.put("nihtbsadnessparentrptsfages37", "par_sadness");
        map.put("nihtbsadnessparentrptcatages812", "par_sadness");
        map.put("nihtbangerparentrptsfages37", "par_anger");
        map.put("nihtbangerparentrptcatages812", "par_anger");
        // Added 2015-02-02 Toolbox update - Words in Noise V2
        map.put("nihtbwordsinnoiseage6v20", "sen_noisev2");
        
        toolboxTestMap = Collections.unmodifiableMap(map);
    }
	
	/**
	 * 
	 * @param listenerControl
	 * @param u
	 * @param session
	 * @param overwrite:   'append' means overwrite, but preserve un-modified content (don't delete anything)
	 *                      'delete' means delete the pre-existing content.
	 * @param additionalValues: should include project (subject and experiment are expected to be found in the archive)
	 */
	public HCPBehavioralImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u);
		this.user=u;
		this.fw=fw;
		this.params=params;
	}
	
	/**
	 * Instantiates a new HCP behavioral importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public HCPBehavioralImporter(UserI u, Map<String, Object> params) {
		this(null, u, null, params);
		/*
		super(null, u, null, params);
		this.user=u;
		this.fw=null;
		this.params=params;
		*/
	}


	@Override
	public List<String> call() throws ClientException, ServerException {
		verifyProject();
		if (params.get("update")!=null && (
					params.get("update").toString().equalsIgnoreCase("true") ||
					params.get("update").toString().equalsIgnoreCase("t") ||
					params.get("update").toString().equalsIgnoreCase("yes") ||
					params.get("update").toString().equalsIgnoreCase("y") 
				)) {
			updateExisting = true;
		} else if (params.get("updateExisting")!=null && (
					params.get("updateExisting").toString().equalsIgnoreCase("true") ||
					params.get("updateExisting").toString().equalsIgnoreCase("t") ||
					params.get("updateExisting").toString().equalsIgnoreCase("yes") ||
					params.get("updateExisting").toString().equalsIgnoreCase("y") 
				)) {
			updateExisting = true;
		}
		if (params.get("verbose")!=null && (
				params.get("verbose").toString().equalsIgnoreCase("true") ||
				params.get("verbose").toString().equalsIgnoreCase("t") ||
				params.get("verbose").toString().equalsIgnoreCase("yes") ||
				params.get("verbose").toString().equalsIgnoreCase("y")
			)) {
			verboseOutput = true;
		} else if (params.get("verboseOutput")!=null && (
				params.get("verboseOutput").toString().equalsIgnoreCase("true") ||
				params.get("verboseOutput").toString().equalsIgnoreCase("t") ||
				params.get("verboseOutput").toString().equalsIgnoreCase("yes") ||
				params.get("verboseOutput").toString().equalsIgnoreCase("y")
			)) {
			verboseOutput = true;
		}
		try {
			final List<String> returnList = Lists.newArrayList();
			if (fw==null) {
				returnList.addAll(getCsvFileFromBuildPathAndProcess(params.get("BuildPath").toString()));
			} else {
				returnList.addAll(saveAndProcessCsvFile());
			}
			returnList.add("IMPORT COMPLETE:  Successfully imported behavioral CSV");
			this.completed("Successfully imported behavioral CSV");
			return returnList;
		} catch (ClientException e) {
			logger.error("",e);
			this.failed(e.getMessage());
			throw e;
		} catch (ServerException e) {
			logger.error("",e);
			this.failed(e.getMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("",e);
			throw new ServerException(e.getMessage(),new Exception());
		}
	}

	private void verifyProject() throws ClientException {
		if (params.get("project") == null) {
			clientFailed("ERROR:  project parameter must be supplied for import");
		}
		String projID=params.get("project").toString();
		// NOTE:  Changing preload parameter to true.  Required to run via automation script
		proj=AutoXnatProjectdata.getXnatProjectdatasById(projID, user, true);
		if (proj == null) {
			clientFailed("ERROR:  Project specified is invalid or user does not have access to project");
		}
	}
	

	private void clientFailed(String fmsg) throws ClientException {
		this.failed(fmsg);
		throw new ClientException(fmsg,new Exception());
	}

	private List<String> getCsvFileFromBuildPathAndProcess(String buildPath) throws ClientException,ServerException {
		
		if (buildPath==null) {
			clientFailed("ERROR:  Build path was not specified");
		}
		File buildDir = new File(buildPath);
		if (!buildDir.isDirectory()) {
			clientFailed("ERROR:  Build path does not exist or is not a directory");
		}
		Collection<File> fileList = FileUtils.listFiles(buildDir, TrueFileFilter.TRUE, TrueFileFilter.TRUE);
		if (fileList.size()<1) {
			clientFailed("ERROR:  No file has been specified");
		}
		if (fileList.size()>1) {
			clientFailed("ERROR:  This importer supports processing only one file per upload");
		}
		// Iterate over CSV file, optionally updating records and saving new ones
		for (File f : fileList) {
			parseCsvFile(f);
		}
		return returnList;
		
	}

	private List<String> saveAndProcessCsvFile() throws ClientException,ServerException {
		
		String cachePath = ArcSpecManager.GetInstance().getGlobalCachePath();
		Date d = Calendar.getInstance().getTime();
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ("yyyyMMdd_HHmmss");
		String uploadID = formatter.format(d);
		
		// Save input file to cache space
		cachePath+="user_uploads/"+user.getID() + "/" + uploadID + "/";
		final File cacheLoc = new File(cachePath);
		cacheLoc.mkdirs();
		
		final String fileName = fw.getName();
		//String writeout = null;
		File cacheFile = new File(cacheLoc,fileName);
		try {
			//StringWriter writer = new StringWriter();
			FileWriter writer = new FileWriter(cacheFile);
			IOUtils.copy(fw.getInputStream(), writer);
			writer.close();
		} catch (IOException e) {
			throw new ServerException("Could not save CSV file",e);
		}
		
		// Iterate over CSV file, optionally updating records and saving new ones
		parseCsvFile(cacheFile);
		
		return returnList;
		
	}

	private void parseCsvFile(File cacheFile) throws ClientException, ServerException {
		List<Map<String,String>> csvRep = null;
		try {
			csvRep = CsvParser.parseCSV(cacheFile);
		} catch (IOException e) {
			throw new ServerException("Could not parse input CSV file",e);
		}
		if (csvRep==null || csvRep.size()<1) {
			throw new ClientException("Uploaded file contains no data");
		}

        if (csvRep.get(0).containsKey("subid")) {
            populateAsrIdList();
            populateNeoIdList();
            populateNtIdList();
            processNonToolboxData(csvRep);
        }
        else if (csvRep.get(0).containsKey("PIN")) {
            populateToolboxIdList();
            processToolboxData(csvRep);
        }
        else if (csvRep.get(0).containsKey("PUBLIC_ID")) {
            populateSsagaIdList();
            processSsagaData(csvRep);
        }
        else {
            throw new ClientException("Uploaded file is not a known behavioral format");
        }
	}

    private void processNonToolboxData(List<Map<String,String>> csvRep) throws ClientException, ServerException {
        // Loop over rows, creating items
        int rowCount = 0;

        for (Map<String,String> row : csvRep) {
            rowCount++;
            // Pull SubjectID
            String subjLbl = null;
            if (row.containsKey("subid")) {  // *Indicates NonToolbox CSV
                subjLbl = row.get("subid");
                // Retest project records use a prepended subject label
                if (proj.getId().equals(RETEST_PROJECT)) {
                    if (!subjLbl.startsWith(RETEST_ID_PREPEND)) {
                        if (verboseOutput) {
                            returnList.add("Subject record " + subjLbl + " not a retest record.  Skipping...");
                        }
                        continue;
                    } else {
                        if (verboseOutput) {
                            returnList.add("Subject record " + subjLbl + " is a retest record - continue with subject label as " + subjLbl.substring(RETEST_ID_PREPEND.length()));
                        }
                        subjLbl = subjLbl.substring(RETEST_ID_PREPEND.length());
                    }
                }
                if (csvSubjLbls.contains(subjLbl)) {
                    returnList.add("WARNING:  CSV file contains multiple records for " + subjLbl + ".  The last record will replace any information saved by the former.");
                } else {
                    csvSubjLbls.add(subjLbl);
                }
            }
            // Pull Experiment Date
            String dateStr = null;
            Date dateVar = null;
            if (row.containsKey("testdate")) {
                dateStr = row.get("testdate");
                String[] dateArr = dateStr.split("[.\\-/]");
                try {
                    Calendar cal = Calendar.getInstance();
                    if (Integer.parseInt(dateArr[0])>1900) {
                        cal.set(Integer.parseInt(dateArr[0]),Integer.parseInt(dateArr[1])-1,Integer.parseInt(dateArr[2]),0,0,0);
                    } else if (Integer.parseInt(dateArr[2])>1900) {
                        cal.set(Integer.parseInt(dateArr[2]),Integer.parseInt(dateArr[0])-1,Integer.parseInt(dateArr[1]),0,0,0);
                    }
                    dateVar = cal.getTime();
                } catch(Exception e) {
                    returnList.add("WARNING:  Could not set experiment date (SUBJECT=" + subjLbl + ").");
                }
            }
            if (dateVar == null) {
                throw new ClientException("Invalid CSV file format.  Column \"testdate\", containing date of experiment, is not found");
            }

            // Process Data
            // NOTE:  Changing preload parameter to true.  Required to run via automation script
            XnatSubjectdata sub = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(),subjLbl,user, false);
            if (sub == null) {
                // Modified Per Cindy 2012-08-25 (File sometimes has Phase I subjects and should not fail when subject not found);
                //throw new ClientException("Could not save ASR record - Subject " + subjLbl + " not found under project " + proj.getId());
                returnList.add("WARNING:  Subject " + subjLbl + " not found under project " + proj.getId() + " - could not save record.");
                continue;
            }


            if (dbAsrSubjLbls.contains(subjLbl) && !updateExisting) {
                if (verboseOutput) {
                    returnList.add("Subject " + subjLbl + " already exists in ASR database and update=false.  Skipping...");
                }
            } else if (row.get("ASR_test").equals("")) {  // Skip subjects that don't have ASR data
                if (verboseOutput)
                    returnList.add("No ASR data for " + sub.getLabel() + ". Skipping...");
            } else {
                populateAndSaveOrUpdateAsr(row,sub,rowCount,dateVar);
            }

            if (dbNeoSubjLbls.contains(subjLbl) && !updateExisting) {
                if (verboseOutput) {
                    returnList.add("Subject " + subjLbl + " already exists in NEO database and update=false.  Skipping...");
                }
            } else if (row.get("NEO.NEO").equals("")) {  // Skip subjects that don't have NEO data
                if (verboseOutput)
                    returnList.add("No NEO data for " + sub.getLabel() + ". Skipping...");
            } else {
                populateAndSaveOrUpdateNeo(row,sub,rowCount,dateVar);
            }

            if (dbNtSubjLbls.contains(subjLbl) && !updateExisting) {
                if (verboseOutput) {
                    returnList.add("Subject " + subjLbl + " already exists in GUR database and update=false.  Skipping...");
                }
            } else {
                populateAndSaveOrUpdateNt(row,sub,rowCount,dateVar);
            }
        }
    }

    private void processToolboxData(List<Map<String,String>> csvRep) throws ClientException, ServerException {
        String curSubj = null;
        String curAssmnt = null;
        String firstPIN = csvRep.get(1).get("PIN");
        // Initialize prev vars so the outer condition fails on the first iteration
        String prevRowSubj = firstPIN.substring(3, firstPIN.length());
        String prevRowAssmnt = csvRep.get(1).get("Assmnt");
        Map<String,String> record = new HashMap<String,String>();
        boolean isRetestProj = proj.getId().equals(RETEST_PROJECT) ? true : false;
        boolean isRetestSubj = false;

        for (Map<String,String> row : csvRep) {
            //if (!row.get("Consent").equals("1"))
            //    continue;

            curSubj = row.get("PIN");
            curSubj = curSubj.substring(3, curSubj.length());
            curAssmnt = row.get("Assmnt");

            // Once a new subject PIN or first row of a retest record is reached, process the full record
            if (!curSubj.equals(prevRowSubj) || !curAssmnt.equals(prevRowAssmnt)) {
                // Get the previous row's subject, which is the record that was just built
            	// NOTE:  Changing preload parameter to true.  Required to run via automation script
                XnatSubjectdata sub = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(),prevRowSubj,user,false);

                if (prevRowSubj.length() != 6) {
                    if (verboseOutput)
                        returnList.add("INFO: Subject " + prevRowSubj + " is a test record and will not be saved.");
                } else if (sub == null) {
                    returnList.add("WARNING: Subject " + prevRowSubj + " not found under project " + proj.getId() + " - could not save record.");
                } else if ((!isRetestProj && !isRetestSubj) || (isRetestProj && isRetestSubj)) {
                    if (dbTbSubjLbls.contains(prevRowSubj) && !updateExisting) {
                        if (verboseOutput) {
                            returnList.add("INFO: Subject " + prevRowSubj + " already exists in the Toolbox database and update=false. Skipping...");
                        }
                    } else {
                        record.put("assmnt_num", prevRowAssmnt);
                        populateAndSaveOrUpdateTb(record, sub);
                    }
                }
                // Courtesy warning per Cindy's request
                String walkTimeStr = record.get("mot_walk_4m_csrc");
                if (walkTimeStr != null) {
                    double walkTime = Double.parseDouble(walkTimeStr);
                    if (walkTime < 0.5 || walkTime > 3.0)
                        returnList.add("WARNING: 4-meter walk time for " + prevRowSubj + " is out of range (0.5 - 3.0). Check for accuracy.");
                }
                // Record is processed. Start collecting the next set of rows.
                record.clear();
                isRetestSubj = false;
            }
            // add current row to record
            String testName = row.get("Form").toLowerCase().replaceAll("[^a-zA-Z0-9]","");
            String mapping = toolboxTestMap.get(testName);

            if (mapping == null) { // Indicates a previously unseen Toolbox form name as of 11-1-2013
                throw new ClientException("ERROR: Change detected in CSV format. Test \"" + row.get("Form") + "\" could not be found. Upload failed.");
            } else if (mapping.equals("unused")) {
                ; // Do nothing
//            } else if (row.get("Form").contains("Parent")) {
//                // Lifespan Parent Forms contain different score types
//                String ageAdjusted = row.get("Age Adjusted Scale Score");
//                String fullyAdjusted = row.get("Fully Adjusted Scale Score");
//                String nationalPercentile = row.get("National Percentile (age adjusted)");
//
//                record.put(mapping + "_age-adj", ageAdjusted);
//                record.put(mapping + "_fully-adj", fullyAdjusted);
//                record.put(mapping + "_percent", nationalPercentile);
            } else {
                String tScore = row.get("TScore");
                String computedScore = row.get("Computed Score");
                String unadjustedScore = row.get("Unadjusted Scale Score");
                String adjustedScore = row.get("Age Adjusted Scale Score");

                record.put(mapping + "_tscr", tScore);
                record.put(mapping + "_cscr", computedScore);
                record.put(mapping + "_uscr", unadjustedScore);
                record.put(mapping + "_ascr", adjustedScore);
            }
            // Must check if retest sub at the end since the subject isn't processed until a new PIN is reached
            if (row.get("Assmnt").equals("2") && prevRowAssmnt.equals("1")) {
                isRetestSubj = true;
            }
            prevRowSubj = curSubj;
            prevRowAssmnt = curAssmnt;
        }
        // Process the last record
        // NOTE:  Changing preload parameter to true.  Required to run via automation script
        XnatSubjectdata sub = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(),prevRowSubj,user,false);
        if (prevRowSubj.length() != 6) {
            if (verboseOutput)
                returnList.add("INFO: Subject " + prevRowSubj + " is a test record and will not be saved.");
        } else if (sub == null) {
            returnList.add("WARNING: Subject " + prevRowSubj + " not found under project " + proj.getId() + " - could not save record.");
        } else if ((!isRetestProj && !isRetestSubj) || (isRetestProj && isRetestSubj)) {
            if (dbTbSubjLbls.contains(prevRowSubj) && !updateExisting) {
                if (verboseOutput) {
                    returnList.add("INFO: Subject " + prevRowSubj + " already exists in the Toolbox database and update=false. Skipping...");
                }
            } else {
                record.put("assmnt_num", prevRowAssmnt);
                populateAndSaveOrUpdateTb(record, sub);
            }
        }
    }

    private void processSsagaData(List<Map<String,String>> csvRep) throws ClientException, ServerException {
        // Loop over rows, creating items
        int rowCount = 0;

        for (Map<String,String> row : csvRep) {
            rowCount++;
            // Pull SubjectID
            String subjLbl = null;
            if (row.containsKey("PUBLIC_ID")) {
                subjLbl = row.get("PUBLIC_ID");
                // Retest project records use a prepended subject label
                if (proj.getId().equals(RETEST_PROJECT)) {
                    if (!subjLbl.startsWith(RETEST_ID_PREPEND)) {
                        if (verboseOutput) {
                            returnList.add("Subject record " + subjLbl + " not a retest record.  Skipping...");
                        }
                        continue;
                    } else {
                        if (verboseOutput) {
                            returnList.add("Subject record " + subjLbl + " is a retest record - continue with subject label as " + subjLbl.substring(RETEST_ID_PREPEND.length()));
                        }
                        subjLbl = subjLbl.substring(RETEST_ID_PREPEND.length());
                    }
                }
                if (csvSubjLbls.contains(subjLbl)) {
                    returnList.add("WARNING:  CSV file contains multiple records for " + subjLbl + ".  The last record will replace any information saved by the former.");
                } else {
                    csvSubjLbls.add(subjLbl);
                }
            }

            // Process Data
            // NOTE:  Changing preload parameter to true.  Required to run via automation script
            XnatSubjectdata sub = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjLbl, user, false);
            if (sub == null) {
                returnList.add("WARNING:  Subject " + subjLbl + " not found under project " + proj.getId() + " - could not save record.");
                continue;
            }

            // Replace all '.' with empty string
            for (String col : row.keySet()) {
                if (row.get(col).equals("."))
                    row.put(col, "");
            }

            if (dbSsagaSubjLbls.contains(subjLbl) && !updateExisting) {
                if (verboseOutput) {
                    returnList.add("Subject " + subjLbl + " already exists in SSAGA database and update=false. Skipping...");
                }
            } else {
                populateAndSaveOrUpdateSsaga(row, sub, rowCount);
            }
        }
    }

	private void populateAsrIdList() {
        CriteriaCollection cc;
        cc=new CriteriaCollection("OR");
        cc.addClause(NtAsr.SCHEMA_ELEMENT_NAME + "/project", proj.getId());
        cc.addClause(NtAsr.SCHEMA_ELEMENT_NAME + "/sharing/share/project", proj.getId());
        // NOTE:  Changing preload parameter to true.  Required to run via automation script
		ArrayList<NtAsr> asrRawRecords=NtAsr.getNtAsrsByField(cc, user, false);
		for (NtAsr record : asrRawRecords) {
			String thisId = record.getSubjectData().getLabel();
			if (dbAsrSubjLbls.contains(thisId)) {
				returnList.add("WARNING:  ASR Raw data contains multiple records for " + thisId);
			} else {
				dbAsrSubjLbls.add(thisId);
			}
		}
		asrRawRecords = null;
	}

	private void populateNeoIdList() {
        CriteriaCollection cc;
        cc=new CriteriaCollection("OR");
        cc.addClause(NtNeo.SCHEMA_ELEMENT_NAME + "/project", proj.getId());
        cc.addClause(NtNeo.SCHEMA_ELEMENT_NAME + "/sharing/share/project", proj.getId());
        // NOTE:  Changing preload parameter to true.  Required to run via automation script
		ArrayList<NtNeo> neoRawRecords=NtNeo.getNtNeosByField(cc, user, false);
		for (NtNeo record : neoRawRecords) {
			String thisId = record.getSubjectData().getLabel();
			if (dbNeoSubjLbls.contains(thisId)) {
				returnList.add("WARNING:  NEO Raw data contains multiple records for " + thisId);
			} else {
				dbNeoSubjLbls.add(thisId);
			}
		}
		neoRawRecords = null;
	}

	private void populateNtIdList() {
        CriteriaCollection cc;
        cc=new CriteriaCollection("OR");
        cc.addClause(NtScores.SCHEMA_ELEMENT_NAME + "/project", proj.getId());
        cc.addClause(NtScores.SCHEMA_ELEMENT_NAME + "/sharing/share/project", proj.getId());
        // NOTE:  Changing preload parameter to true.  Required to run via automation script
		ArrayList<NtScores> gurScoringRecords=NtScores.getNtScoressByField(cc, user, false);
		for (NtScores record : gurScoringRecords) {
			String thisId = record.getSubjectData().getLabel();
			if (dbNtSubjLbls.contains(thisId)) {
				returnList.add("WARNING:  GUR Scoring data contains multiple records for " + thisId);
			} else {
				dbNtSubjLbls.add(thisId);
			}
		}
		gurScoringRecords = null;
	}

    private void populateToolboxIdList() {
        CriteriaCollection cc;
        cc = new CriteriaCollection("OR");
        cc.addClause(HcpToolboxdata.SCHEMA_ELEMENT_NAME + "/project", proj.getId());
        cc.addClause(HcpToolboxdata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", proj.getId());
        // NOTE:  Changing preload parameter to true.  Required to run via automation script
        ArrayList<HcpToolboxdata> tbScoringRecords = HcpToolboxdata.getHcpToolboxdatasByField(cc, user, false);
        for (HcpToolboxdata record : tbScoringRecords) {
            String thisId = record.getSubjectData().getLabel();
            if (dbTbSubjLbls.contains(thisId)) {
                returnList.add("WARNING: Toolbox Scoring data contains multiple records for " + thisId);
            } else {
                dbTbSubjLbls.add(thisId);
            }
        }
        tbScoringRecords = null;
    }

    private void populateSsagaIdList() {
        CriteriaCollection cc;
        cc = new CriteriaCollection("OR");
        cc.addClause(HcpSsagadata.SCHEMA_ELEMENT_NAME + "/project", proj.getId());
        cc.addClause(HcpSsagadata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", proj.getId());
        // NOTE:  Changing preload parameter to true.  Required to run via automation script
        ArrayList<HcpSsagadata> ssagaRecords = HcpSsagadata.getHcpSsagadatasByField(cc, user, false);
        for (HcpSsagadata record : ssagaRecords) {
            String thisId = record.getSubjectData().getLabel();
            if (dbSsagaSubjLbls.contains(thisId)) {
                returnList.add("WARNING: SSAGA data contains multiple records for " + thisId);
            } else {
                dbSsagaSubjLbls.add(thisId);
            }
        }
        // ssagaRecords = null;
    }

	@SuppressWarnings({ "static-access", "unchecked" })
	private void populateAndSaveOrUpdateAsr(Map<String, String> row, XnatSubjectdata sub, int rowCount, Date dateVar) throws ServerException, ClientException {
		////////////////////////////////////////////
		// Create and save ASR RawData assessment //
		////////////////////////////////////////////
		
		try {
			// NOTE:  Assumption of one record per subject very important here.
			CriteriaCollection cc=new CriteriaCollection("AND");
			cc.addClause(NtAsr.SCHEMA_ELEMENT_NAME + "/project",proj.getId());
			cc.addClause(NtAsr.SCHEMA_ELEMENT_NAME + "/subject_ID", sub.getId());
            // NOTE:  Changing preload parameter to true.  Required to run via automation script
			ArrayList<NtAsr> currList = NtAsr.getNtAsrsByField(cc, user, false);
			NtAsr currentAsr = (currList.size()>0) ? currList.get(0) : null;
			NtAsr asrRaw = (currentAsr!=null) ? currentAsr : new NtAsr((UserI)user);

            // The following need not be assigned for pre-existing records
			if (currentAsr==null) {
				if (!proj.getId().equals(RETEST_PROJECT)) {
					asrRaw.setLabel(sub.getLabel() + "_asr");
				} else {
					asrRaw.setLabel(sub.getLabel() + "_asr_rt");
				} 
				asrRaw.setId(XnatExperimentdata.CreateNewID());
				asrRaw.setProject(proj.getId());
				asrRaw.setSubjectId(sub.getId());
			} 
		
			asrRaw.setDate(dateVar);
			for (GenericWrapperField field : (ArrayList<GenericWrapperField>)asrRaw.getItem().getGenericSchemaElement().getAllFields()) {
				String rowField = asr_prepend + field.getName();
				if (row.containsKey(rowField)) {
					try {
						asrRaw.setProperty(asrRaw.SCHEMA_ELEMENT_NAME + "/" + field.getXMLPathString(), row.get(rowField));
					} catch (XFTInitException e) {
						// Do nothing for now
					} catch (FieldNotFoundException e) {
						// Do nothing for now
					} catch (InvalidValueException e) {
						// Do nothing for now
					}
				}
			}
			try {
				String actionV = null;
				if (currentAsr!=null) {
					if (verboseOutput) 
						returnList.add("Updating ASR record for " + sub.getLabel());
					actionV = EventUtils.MODIFY_VIA_WEB_SERVICE;
				} else {
					if (verboseOutput) 
						returnList.add("Saving new ASR record for " + sub.getLabel());
					actionV = EventUtils.CREATE_VIA_WEB_SERVICE;
				}
				final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, asrRaw.getItem(),
							EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, actionV, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(asrRaw,user,false,true,ci)) {
					PersistentWorkflowUtils.complete(wrk, ci);
				} else {
					PersistentWorkflowUtils.fail(wrk,ci);
					if (verboseOutput) 
						returnList.add("No change from pre-existing ASR record - modification not required - (SUBJECT=" + sub.getLabel() + ")");
				}
				newAsrSubjLbls.add(sub.getLabel());
			} catch (Exception e) {
				throw new ServerException("Could not save ASR record - (SUBJECT=" + sub.getLabel() + ")",e);
			}
		} catch (Exception e) {
			throw new ServerException("Could not process ASR record - (SUBJECT=" + sub.getLabel() + ")",e);
		}
		
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private void populateAndSaveOrUpdateNeo(Map<String, String> row, XnatSubjectdata sub, int rowCount, Date dateVar) throws ClientException, ServerException {
		////////////////////////////////////////////
		// Create and save NEO RawData assessment //
		////////////////////////////////////////////
		try {
			
			// NOTE:  Assumption of one record per subject very important here.
			CriteriaCollection cc=new CriteriaCollection("AND");
			cc.addClause(NtNeo.SCHEMA_ELEMENT_NAME + "/project",proj.getId());
			cc.addClause(NtNeo.SCHEMA_ELEMENT_NAME + "/subject_ID", sub.getId());
            // NOTE:  Changing preload parameter to true.  Required to run via automation script
			ArrayList<NtNeo> currList = NtNeo.getNtNeosByField(cc, user, false);
			NtNeo currentNeo = (currList.size()>0) ? currList.get(0) : null;
			NtNeo neoRaw = (currentNeo!=null) ? currentNeo : new NtNeo((UserI)user);
	
			// The following need not be assigned for pre-existing records
			if (currentNeo==null) {
				if (!proj.getId().equals(RETEST_PROJECT)) {
					neoRaw.setLabel(sub.getLabel() + "_neo");
				} else {
					neoRaw.setLabel(sub.getLabel() + "_neo_rt");
				} 
				neoRaw.setId(XnatExperimentdata.CreateNewID());
				neoRaw.setProject(proj.getId());
				neoRaw.setSubjectId(sub.getId());
			} 
		
			neoRaw.setDate(dateVar);
			for (GenericWrapperField field : (ArrayList<GenericWrapperField>)neoRaw.getItem().getGenericSchemaElement().getAllFields()) {
				String rowField = neo_prepend + field.getName();
				if (row.containsKey(rowField)) {
					try {
						neoRaw.setProperty(neoRaw.SCHEMA_ELEMENT_NAME + "/" + field.getXMLPathString(), row.get(rowField));
					} catch (XFTInitException e) {
						// Do nothing for now
					} catch (FieldNotFoundException e) {
						// Do nothing for now
					} catch (InvalidValueException e) {
						// Do nothing for now
					}
				}
			}
			
			try {
				String actionV = null;
				if (currentNeo!=null) {
					if (verboseOutput) 
						returnList.add("Updating NEO record for " + sub.getLabel());
					actionV = EventUtils.MODIFY_VIA_WEB_SERVICE;
				} else {
					if (verboseOutput) 
						returnList.add("Saving new NEO record for " + sub.getLabel());
					actionV = EventUtils.CREATE_VIA_WEB_SERVICE;
				}
				final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, neoRaw.getItem(),
							EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, actionV, null, null));
				final EventMetaI ci = wrk.buildEvent();
				
				if (SaveItemHelper.authorizedSave(neoRaw,user,false,true,ci)) {
					PersistentWorkflowUtils.complete(wrk, ci);
				} else {
					PersistentWorkflowUtils.fail(wrk,ci);
					if (verboseOutput) 
						returnList.add("No change from pre-existing NEO record - modification not required - (SUBJECT=" + sub.getLabel() + ")");
				}
				
				newNeoSubjLbls.add(sub.getLabel());
			} catch (Exception e) {
				throw new ServerException("Could not save NEO record - (SUBJECT=" + sub.getLabel() + ")",e);
			}
		} catch (Exception e) {
			throw new ServerException("Could not process NEO record - (SUBJECT=" + sub.getLabel() + ")",e);
		}
		
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private void populateAndSaveOrUpdateNt(Map<String, String> row, XnatSubjectdata sub, int rowCount, Date dateVar) throws ClientException, ServerException {
		////////////////////////////////////////////
		// Create and save GUR Scoring assessment //
		////////////////////////////////////////////
		
		try {
			
			// NOTE:  Assumption of one record per subject very important here.
			CriteriaCollection cc=new CriteriaCollection("AND");
			cc.addClause(NtScores.SCHEMA_ELEMENT_NAME + "/project",proj.getId());
			cc.addClause(NtScores.SCHEMA_ELEMENT_NAME + "/subject_ID", sub.getId());
            // NOTE:  Changing preload parameter to true.  Required to run via automation script
			ArrayList<NtScores> currList = NtScores.getNtScoressByField(cc, user, false);
			NtScores currentNt = (currList.size()>0) ? currList.get(0) : null;
			NtScores ntRaw = (currentNt!=null) ? currentNt : new NtScores((UserI)user);
		
			// The following need not be assigned for pre-existing records
			if (currentNt==null) {
				if (!proj.getId().equals(RETEST_PROJECT)) {
					ntRaw.setLabel(sub.getLabel() + "_nontoolbox");
				} else {
					ntRaw.setLabel(sub.getLabel() + "_nontoolbox_rt");
				}
				ntRaw.setId(XnatExperimentdata.CreateNewID());
				ntRaw.setProject(proj.getId());
				ntRaw.setSubjectId(sub.getId());
			} 
		
			ntRaw.setDate(dateVar);
			// Generate asr score values
			Map<String,String> asrScores = NontoolboxAsrScoringUtil.computeScores(row);
			// Loop over fields
			fieldLoop:
			for (GenericWrapperField field : (ArrayList<GenericWrapperField>)ntRaw.getItem().getGenericSchemaElement().getAllFields()) {
				String rowField = "." + field.getName();
				// Assign non-ASR values
				for (String keyField : row.keySet()) {
					if (keyField.endsWith(rowField)) {
						try {
							if (!er40Map.keySet().contains(keyField)) {
								ntRaw.setProperty(ntRaw.SCHEMA_ELEMENT_NAME + "/" + field.getXMLPathString(), row.get(keyField));
							} else {
								// Handling for ER40 kids columns
								if (row.get(keyField) != null && !(row.get(keyField).trim().length()<1)) {
									ntRaw.setProperty(ntRaw.SCHEMA_ELEMENT_NAME + "/" + field.getXMLPathString(), row.get(keyField));
								} else {
									ntRaw.setProperty(ntRaw.SCHEMA_ELEMENT_NAME + "/" + field.getXMLPathString(), row.get(er40Map.get(keyField)));
								}
							}
						} catch (XFTInitException e) {
							// Do nothing for now
						} catch (FieldNotFoundException e) {
							// Do nothing for now
						} catch (InvalidValueException e) {
							// Do nothing for now
						}
						continue fieldLoop;
					}
				}
				// Assign ASR values
				for (String scoreField : asrScores.keySet()) {
					if (rowField.endsWith(scoreField)) {
						try {
							ntRaw.setProperty(ntRaw.SCHEMA_ELEMENT_NAME + "/" + field.getXMLPathString(), asrScores.get(scoreField));
						} catch (XFTInitException e) {
							// Do nothing for now
						} catch (FieldNotFoundException e) {
							// Do nothing for now
						} catch (InvalidValueException e) {
							// Do nothing for now
						}
						continue fieldLoop;
					}
				}
			}
			try {
				String actionV = null;
				if (currentNt!=null) {
					if (verboseOutput) 
						returnList.add("Updating Non-Toolbox Scoring record for " + sub.getLabel());
					actionV = EventUtils.MODIFY_VIA_WEB_SERVICE;
				} else {
					if (verboseOutput) 
						returnList.add("Saving new Non-Toolbox Scoring record for " + sub.getLabel());
					actionV = EventUtils.CREATE_VIA_WEB_SERVICE;
				}
				final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, ntRaw.getItem(),
							EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, actionV, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(ntRaw,user,false,true,ci)) {
					PersistentWorkflowUtils.complete(wrk, ci);
				} else {
					PersistentWorkflowUtils.fail(wrk,ci);
					if (verboseOutput) 
						returnList.add("No change from pre-existing GUR Scoring record - modification not required - (SUBJECT=" + sub.getLabel() + ")");
				}
				
				newNtSubjLbls.add(sub.getLabel());
			} catch (Exception e) {
				throw new ServerException("Could not save GUR Scoring record - (SUBJECT=" + sub.getLabel() + ")",e);
			}
		} catch (Exception e) {
			throw new ServerException("Could not process GUR Scoring record - (SUBJECT=" + sub.getLabel() + ")",e);
		}
		
	}

    @SuppressWarnings({ "static-access", "unchecked" })
    private void populateAndSaveOrUpdateTb(Map <String,String> row, XnatSubjectdata sub) throws ClientException, ServerException {

        try {
            CriteriaCollection cc = new CriteriaCollection("AND");
            cc.addClause(HcpToolboxdata.SCHEMA_ELEMENT_NAME + "/project",proj.getId());
            cc.addClause(HcpToolboxdata.SCHEMA_ELEMENT_NAME + "/subject_ID", sub.getId());
            // NOTE:  Changing preload parameter to true.  Required to run via automation script
            ArrayList<HcpToolboxdata> currList = HcpToolboxdata.getHcpToolboxdatasByField(cc, user, false);
            HcpToolboxdata currentTb = (currList.size()>0) ? currList.get(0) : null;
            HcpToolboxdata tbScore = (currentTb!=null) ? currentTb : new HcpToolboxdata((UserI)user);

            // The following need not be assigned for pre-existing records
            if (currentTb == null) {
                if (proj.getId().equals(RETEST_PROJECT)) {
                    tbScore.setLabel(sub.getLabel() + "_toolbox_rt");
                } else {
                    tbScore.setLabel(sub.getLabel() + "_toolbox");
                }
                tbScore.setId(XnatExperimentdata.CreateNewID());
                tbScore.setProject(proj.getId());
                tbScore.setSubjectId(sub.getId());
            }
            for (GenericWrapperField field : (ArrayList<GenericWrapperField>)tbScore.getItem().getGenericSchemaElement().getAllFields()) {
                String rowField = field.getName();
                if (row.containsKey(rowField)) {
                    try {
                        tbScore.setProperty(tbScore.SCHEMA_ELEMENT_NAME + "/" + field.getXMLPathString(), row.get(rowField));
                    } catch (XFTInitException e) {
                        // Do nothing for now
                    } catch (FieldNotFoundException e) {
                        // Do nothing for now
                    } catch (InvalidValueException e) {
                        // Do nothing for now
                    }
                }
            }
            try {
                String actionV = null;
                if (currentTb != null) {
                    if (verboseOutput)
                        returnList.add("Updating Toolbox record for " + sub.getLabel());
                    actionV = EventUtils.MODIFY_VIA_WEB_SERVICE;
                } else {
                    if (verboseOutput)
                        returnList.add("Saving new Toolbox record for " + sub.getLabel());
                    actionV = EventUtils.CREATE_VIA_WEB_SERVICE;
                }
                final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, tbScore.getItem(),
                        EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, actionV, null, null));
                final EventMetaI ci = wrk.buildEvent();

                if (SaveItemHelper.authorizedSave(tbScore,user,false,true,ci)) {
                    PersistentWorkflowUtils.complete(wrk, ci);
                } else {
                    PersistentWorkflowUtils.fail(wrk,ci);
                    if (verboseOutput)
                        returnList.add("No change from pre-existing Toolbox record - modification not required - (SUBJECT=" + sub.getLabel() + ")");
                }
                newTbSubjLbls.add(sub.getLabel());
            } catch (Exception e) {
                throw new ServerException("Could not save Toolbox record - (SUBJECT=" + sub.getLabel() + ")",e);
            }
        } catch (Exception e) {
            throw new ServerException("Could not process Toolbox Scoring record - (SUBJECT=" + sub.getLabel() + ")",e);
        }
    }

    @SuppressWarnings({ "static-access", "unchecked" })
    private void populateAndSaveOrUpdateSsaga(Map<String, String> row, XnatSubjectdata sub, int rowCount) throws ClientException, ServerException {
        ////////////////////////////////////////////
        // Create and save SSAGA Scoring assessment //
        ////////////////////////////////////////////

        try {
            // NOTE:  Assumption of one record per subject very important here.
            CriteriaCollection cc = new CriteriaCollection("AND");
            cc.addClause(HcpSsagadata.SCHEMA_ELEMENT_NAME + "/project",proj.getId());
            cc.addClause(HcpSsagadata.SCHEMA_ELEMENT_NAME + "/subject_ID", sub.getId());
            // NOTE:  Changing preload parameter to true.  Required to run via automation script
            ArrayList<HcpSsagadata> currList = HcpSsagadata.getHcpSsagadatasByField(cc, user, false);
            HcpSsagadata currentSsaga = (currList.size() > 0) ? currList.get(0) : null;
            HcpSsagadata ssagaRecord = (currentSsaga != null) ? currentSsaga : new HcpSsagadata((UserI)user);

            // The following need not be assigned for pre-existing records
            if (currentSsaga == null) {
                if (proj.getId().equals(RETEST_PROJECT)) {
                    ssagaRecord.setLabel(sub.getLabel() + "_ssaga_rt");
                } else {
                    ssagaRecord.setLabel(sub.getLabel() + "_ssaga");
                }
                ssagaRecord.setId(XnatExperimentdata.CreateNewID());
                ssagaRecord.setProject(proj.getId());
                ssagaRecord.setSubjectId(sub.getId());
            }

            for (GenericWrapperField field : (ArrayList<GenericWrapperField>)ssagaRecord.getItem().getGenericSchemaElement().getAllFields()) {
                String rowField = field.getName();
                if (row.containsKey(rowField)) {
                    try {
                        ssagaRecord.setProperty(ssagaRecord.SCHEMA_ELEMENT_NAME + "/" + field.getXMLPathString(), row.get(rowField));
                    } catch (XFTInitException e) {
                        // Do nothing for now
                    } catch (FieldNotFoundException e) {
                        // Do nothing for now
                    } catch (InvalidValueException e) {
                        // Do nothing for now
                    }
                }
            }

            try {
                String actionV = null;
                if (currentSsaga != null) {
                    if (verboseOutput)
                        returnList.add("Updating SSAGA Scoring record for " + sub.getLabel());
                    actionV = EventUtils.MODIFY_VIA_WEB_SERVICE;
                } else {
                    if (verboseOutput)
                        returnList.add("Saving new SSAGA Scoring record for " + sub.getLabel());
                    actionV = EventUtils.CREATE_VIA_WEB_SERVICE;
                }
                final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, ssagaRecord.getItem(),
                        EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, actionV, null, null));
                final EventMetaI ci = wrk.buildEvent();
                if (SaveItemHelper.authorizedSave(ssagaRecord, user, false, true, ci)) {
                    PersistentWorkflowUtils.complete(wrk, ci);
                } else {
                    PersistentWorkflowUtils.fail(wrk,ci);
                    if (verboseOutput)
                        returnList.add("No change from pre-existing SSAGA Scoring record - modification not required - (SUBJECT=" + sub.getLabel() + ")");
                }
                newNtSubjLbls.add(sub.getLabel());
            } catch (Exception e) {
                throw new ServerException("Could not save SSAGA Scoring record - (SUBJECT=" + sub.getLabel() + ")",e);
            }
        } catch (Exception e) {
            throw new ServerException("Could not process SSAGA Scoring record - (SUBJECT=" + sub.getLabel() + ")",e);
        }

    }
}
