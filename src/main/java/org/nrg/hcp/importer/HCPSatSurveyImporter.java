package org.nrg.hcp.importer;

//**********************************************************************************************************************//
// NOTE (2016/10/05):  I believe this importer was never used.  I'm carrying it over, but it likely would never be used //
//**********************************************************************************************************************//

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.CatCatalog;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.auto.AutoXnatProjectdata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectSubjResourceImpl;
import org.nrg.xnat.restlet.actions.importer.ImporterHandler;
import org.nrg.xnat.restlet.actions.importer.ImporterHandlerA;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import com.google.common.collect.Lists;

/**
 * Uploads satisfaction survey into subject resource
 * @author Mike Hodge <hodgem@mir.wustl.edu>
 *
 */
@ImporterHandler(handler = "SatSurvey", allowCallsWithoutFiles = false, callPartialUriWrap = false)
public class HCPSatSurveyImporter extends ImporterHandlerA implements Callable<List<String>> {

	static Logger logger = Logger.getLogger(HCPSatSurveyImporter.class);

	private final FileWriterWrapperI fw;
	private final UserI user;
	final Map<String,Object> params;
   	private XnatProjectdata proj;
   	private XnatSubjectdata subj;
	private DirectSubjResourceImpl subjectModifier;
   	private static final String RESOURCE_LABEL = "SAT_SURVEY";
	private static final String RESOURCE_FORMAT="MISC";
	private static final String RESOURCE_CONTENT="RAW";
	private static final String CATXML_EXT="_catalog.xml";
   	private ArrayList<String> returnList = new ArrayList<String>();
   	
   	
	/**
	 * 
	 * @param listenerControl
	 * @param u
	 * @param session
	 * @param overwrite:   'append' means overwrite, but preserve un-modified content (don't delete anything)
	 *                      'delete' means delete the pre-existing content.
	 * @param additionalValues: should include project (subject and experiment are expected to be found in the archive)
	 */
	public HCPSatSurveyImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u);
		this.user=u;
		this.fw=fw;
		this.params=params;
	}

	@Override
	public List<String> call() throws ClientException, ServerException {
		verifyProject();
		setSubjExptValues();
		final List<String> returnList = Lists.newArrayList();
		try {
			if (fw==null) {
				returnList.addAll(getFileFromBuildPathAndProcess(params.get("BuildPath").toString()));
			} else {
				returnList.addAll(saveAndProcessFile());
			}
			this.completed("Successfully imported Satisfaction Survey");
			return returnList;
		} catch (ClientException e) {
			logger.error("",e);
			this.failed(e.getMessage());
			throw e;
		} catch (ServerException e) {
			logger.error("",e);
			this.failed(e.getMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("",e);
			throw new ServerException(e.getMessage(),new Exception());
		}
	}

	private void verifyProject() throws ClientException {
		if (params.get("project") == null) {
			clientFailed("ERROR:  project parameter must be supplied for import");
		}
		String projID=params.get("project").toString();
		proj=AutoXnatProjectdata.getXnatProjectdatasById(projID, user, false);
		if (proj == null) {
			clientFailed("ERROR:  Project specified is invalid or user does not have access to project");
		}
	}

	private void setSubjExptValues() {
		/*
		if (params.get("experiment")!=null) {
			String expLbl = params.get("experiment").toString();
			if (expLbl!=null) {
				try {
					this.exp = (XnatSubjectassessordata)XnatSubjectassessordata.GetExptByProjectIdentifier(proj.getId(), expLbl, user, false);
				} catch (ClassCastException cce) {
					// Do nothing, expecting subject assessor
				}
			}
		}
		*/
		if (this.subj == null && params.get("subject")!=null) {
			String subjLbl = params.get("subject").toString();
			if (subjLbl!=null) {
				this.subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjLbl, user, false);
	        }
		}
	}
	

	private void clientFailed(String fmsg) throws ClientException {
		this.failed(fmsg);
		throw new ClientException(fmsg,new Exception());
	}

	private List<String> getFileFromBuildPathAndProcess(String buildPath) throws ClientException,ServerException {
		
		if (buildPath==null) {
			clientFailed("ERROR:  Build path was not specified");
		}
		File buildDir = new File(buildPath);
		if (!buildDir.isDirectory()) {
			clientFailed("ERROR:  Build path does not exist or is not a directory");
		}
		Collection<File> fileList = FileUtils.listFiles(buildDir, TrueFileFilter.TRUE, TrueFileFilter.TRUE);
		if (fileList.size()<1) {
			clientFailed("ERROR:  No file has been specified");
		}
		if (fileList.size()>1) {
			clientFailed("ERROR:  This importer supports processing only one file per upload");
		}
		// Iterate over CSV file, optionally updating records and saving new ones
		for (File f : fileList) {
			saveFileToResource(f);
		}
		return returnList;
		
	}

	private List<String> saveAndProcessFile() throws ClientException,ServerException {
		
		String cachePath = ArcSpecManager.GetInstance().getGlobalCachePath();
		Date d = Calendar.getInstance().getTime();
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ("yyyyMMdd_HHmmss");
		String uploadID = formatter.format(d);
		
		// Save input file to cache space
		cachePath+="user_uploads/"+user.getID() + "/" + uploadID + "/";
		final File cacheLoc = new File(cachePath);
		cacheLoc.mkdirs();
		
		final String fileName = fw.getName();
		//String writeout = null;
		File cacheFile = new File(cacheLoc,fileName);
		try {
			//StringWriter writer = new StringWriter();
			FileWriter writer = new FileWriter(cacheFile);
			IOUtils.copy(fw.getInputStream(), writer);
			writer.close();
		} catch (IOException e) {
			throw new ServerException("Could not save file",e);
		}
		
		// Iterate over CSV file, optionally updating records and saving new ones
		saveFileToResource(cacheFile);
		
		return returnList;
		
	}

	private void saveFileToResource(File cacheFile) throws ClientException, ServerException {
		
		try {
			
			String subjLbl = cacheFile.getName().split("[\\W_]")[0];
			XnatSubjectdata thisSubj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjLbl, user, false);
			//XnatSubjectdata thisSubj = XnatSubjectdata.getXnatSubjectdatasById(subjLbl, user, false);
			if (this.subj != null && !this.subj.getLabel().equals(thisSubj.getLabel())) {
				throw new ClientException(cacheFile.getName() + " - Subject identifier in file does not match current subject (FILE=" + subjLbl + ", CURRENT=" + subj.getLabel() + ")");
			}
			if (thisSubj == null) {
				throw new ClientException(cacheFile.getName() + " - Could not process file.  No matching subject. (" + subjLbl + ")");
			}
			// Matching subject, write file to subject resource
			final String eventStr = "Uploaded satisfaction survey for subject " + thisSubj.getLabel() + " (FILE=" + cacheFile.getName() + ")";  
		
			PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, thisSubj.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.ADDED_MISC_FILES, eventStr, null));
		
			final EventMetaI ci = wrk.buildEvent();
		
			final DirectSubjResourceImpl subjectModifier = getSubjectModifier(thisSubj,ci);
		
			createSubjectResourceIfNecessary(thisSubj,subjectModifier);
		
			// Iterate list and add both text and e-prime file
			final ArrayList<StoredFile> fws = new ArrayList<StoredFile>();
			fws.clear();
			fws.add(new StoredFile(cacheFile,false));
			subjectModifier.addFile(fws, RESOURCE_LABEL, null,fws.get(0).getName(), new XnatResourceInfo(user,new Date(),new Date()), false);
			final File edatFile = new File(cacheFile.getParentFile(),cacheFile.getName());
			if (edatFile.exists()) {
				fws.clear();
				fws.add(new StoredFile(edatFile,false));
				subjectModifier.addFile(fws, RESOURCE_LABEL, null,fws.get(0).getName(), new XnatResourceInfo(user,new Date(),new Date()), false);
			}
		
			PersistentWorkflowUtils.complete(wrk, ci);
			returnList.add(eventStr); 
			
		} catch (ClientException e) {
			throw e;
		} catch (Exception e) {
			throw new ClientException("ERROR:  Could not add satisfaction survey files to subject resource (" + e.getMessage() +")");
		}
		
	}
	
	private void createSubjectResourceIfNecessary(final XnatSubjectdata thisSubject,final DirectSubjResourceImpl subjectModifier) throws ClientException {

		final XnatAbstractresourceI resource =  subjectModifier.getResourceByLabel(RESOURCE_LABEL, null);
		if (resource==null) {
			createSubjectResource(thisSubject);
		}
		
	}
	
	private void createSubjectResource(final XnatSubjectdata thisSubject) throws ClientException {
		
		final File resourceDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + File.separator + "subjects" + File.separator +
					thisSubject.getLabel() +  File.separator + RESOURCE_LABEL);
		if (!resourceDir.exists()) {
			resourceDir.mkdirs();
		}
		final File catFile = new File(resourceDir,RESOURCE_LABEL + CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		}
		ecat.setLabel(RESOURCE_LABEL);
		ecat.setFormat(RESOURCE_FORMAT);
		ecat.setContent(RESOURCE_CONTENT);
		// Save resource to session
		final String eventStr = "Resource " + RESOURCE_LABEL + " created under subject " + thisSubject.getId();
		try {
			thisSubject.addResources_resource(ecat);
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, thisSubject.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_RESOURCE, eventStr, null));
			final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(thisSubject,user,false,true,ci)) {
				PersistentWorkflowUtils.complete(wrk, ci);
			} else {
				PersistentWorkflowUtils.fail(wrk,ci);
			}
			returnList.add(eventStr); 
		} catch (ClientException e) {
			throw e;
		} catch (Exception e) {
			throw new ClientException("ERROR:  Couldn't add resource to session - " + e.getMessage(),new Exception());
		}
		
	}

	private DirectSubjResourceImpl getSubjectModifier(final XnatSubjectdata subject,final EventMetaI ci) {
		if (subjectModifier != null) {
			return subjectModifier;
		} else {
			final DirectSubjResourceImpl subjectModifier = new DirectSubjResourceImpl(proj,subject,true,user,ci);
			return subjectModifier;
		}
	}

}

